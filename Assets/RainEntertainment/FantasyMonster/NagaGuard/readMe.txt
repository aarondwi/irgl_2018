an animated Naga Guard with 6 combat animations,perfect for mobile games.

Mesh information: 
NagaGuard 3624 Tris

Texture information:
NagaGuard.png 512x512

Animation list
NagaGuard@atk01.FBX
NagaGuard@atk02.FBX
NagaGuard@die.FBX
NagaGuard@hit.FBX
NagaGuard@idle01.FBX
NagaGuard@run.FBX

Enjoy!