﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneController : MonoBehaviour {

	private GameManager gm;

	void Start(){
		gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}

	void OnMouseUp(){
		gm.SpawnTroops (Input.mousePosition);
	}
}
