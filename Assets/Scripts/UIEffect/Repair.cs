﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Repair : MonoBehaviour {
	public GameObject InfoPanel;
	public Text cost;
	public Text costPanel;
	private GameManager gm;
	// Use this for initialization
	public void Start(){
		gm=GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	public void costText(){
		costPanel.text = cost.text;
	}
	public void Yes(){
		gameObject.transform.position=new Vector3(20000,-12000,0);
		gm.SetBuildingColliderStatus(true);
		int gold = GameObject.Find ("GameManager").GetComponent<GameManager> ().gold;
		if (gold < int.Parse (cost.text.ToString ())) {
			Debug.Log ("tidak cukup");
		} else {
			if (InfoPanel.GetComponent<getBuildingInfo> ()) {
				GameObject.Find ("GameManager").GetComponent<GameManager> ().repairTower (InfoPanel.GetComponent<getBuildingInfo> ().namatower, int.Parse (cost.text.ToString ()));
			} else {
				GameObject.Find ("GameManager").GetComponent<GameManager> ().repairTower (InfoPanel.GetComponent<BarrackInfo> ().namatower, int.Parse (cost.text.ToString ()));
			}
		}
		GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.transform.position=new Vector3(5000,5000,0);
		//GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.gameObject.transform.position=new Vector3(1922,0,0);
	}

	public void No(){
		this.transform.position=new Vector3(2000,-1200,0);
		InfoPanel.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);
	}
}
