﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Sell : MonoBehaviour {
	public GameObject InfoPanel;
	public Text cost;
	public Text costPanel;
	private GameManager gm;
	// Use this for initialization
	public void Start(){
		gm=GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	public void costText(){
		costPanel.text = cost.text;
	}
	public void Yes(){
		gm.SetBuildingColliderStatus(true);
		gameObject.transform.position=new Vector3(20000,-12000,0);
		GameObject.Find ("GameManager").GetComponent<GameManager> ().deleteTower (InfoPanel.GetComponent<getBuildingInfo>().namatower,int.Parse (cost.text.ToString ()));
		GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.transform.position=new Vector3(5000,0,0);
		//GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.gameObject.transform.position=new Vector3(1922,0,0);
	}

	public void No(){
		gameObject.transform.position=new Vector3(2000,-1200,0);
		InfoPanel.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);
	}
}
