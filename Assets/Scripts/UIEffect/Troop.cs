﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Troop : MonoBehaviour {

	public Text[] t;

	public GameObject BarrackPanel;
	public GameObject attackConfirmationPanel;
	public GameObject attackConfirmationPanelButtonList;
	public Text attackConfirmationText;
	public Text capacityText;


	public void Increase(int count){
		
		int a = int.Parse (t [count].text);
		a++;
		if(int.Parse(BarrackPanel.GetComponent<BarrackInfo>().level.text)<2 && (count == 3 || count == 5)){
			a--;
		}
		if(int.Parse(BarrackPanel.GetComponent<BarrackInfo>().level.text)<3 && count == 5){
			a--;
		}
		t [count].text = ""+a;
	}

	public void Decrease(int count){
		int a = int.Parse (t [count].text);
		if(a>0){a--;}else{a = 0;};
		t [count].text = ""+a;
	}

	public void Ok(){
		GameManager gm=GameObject.Find("GameManager").GetComponent<GameManager>();
		int [] jumlah=new int[6];
		int totalCost = 0;
		int temp = 0;
		for (int i = 0; i < 6; i++) {
			jumlah [i] = int.Parse(t[i].text.ToString());
			temp += jumlah [i] * gm.CapacityTroop[i];
			t [i].text = 0.ToString ();
			totalCost += jumlah [i] * gm.BuyTroop [i];
		}

		for (int i = 0; i < gm.troops.Length; i++) {
			temp += gm.troops [i]*gm.CapacityTroop[i];
		}

		if (totalCost > gm.gold) {
			attackConfirmationPanel.SetActive(true);
			attackConfirmationPanelButtonList.SetActive(true);
			attackConfirmationText.text="Not enough gold";
		}
		else if (gm.capacity [gm.buildingList [0].GetComponent<TowerController> ().towerLevel] >= temp) {
			BarrackPanel.GetComponent<BarrackInfo> ().setCapacity (temp);
			gm.plusTroops (jumlah,totalCost);
			this.gameObject.SetActive (false);
			BarrackPanel.SetActive (true);
		} else {
			attackConfirmationPanel.SetActive(true);
			attackConfirmationPanelButtonList.SetActive(true);
			attackConfirmationText.text="Over capacity";
		}
	}
	public void cancelBuy(){
		attackConfirmationPanel.SetActive(false);
	}

	public void Cancel(){
		this.gameObject.SetActive (false);
		BarrackPanel.SetActive (true);
	
	}

}
