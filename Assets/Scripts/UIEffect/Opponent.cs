﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Opponent : MonoBehaviour {
	public GameObject attackPanel;
	public Text[] gold = new Text[9];
	public Text[] gem = new Text[9];
	public Text[] tim = new Text[9];
	private string teamToAttack;
	public GameObject attackConfirmationPanel;
	public GameObject[] attackConfirmationPanelButtonList;
	public Text attackConfirmationText;
	private GameManager gm;
	public string[,] opponent = new string[19,3];

	void Start(){
		gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();

	}

	public void showOpponent(){
		gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();

		for (int i=0; i<19; i++){
			tim[i].text = gm.opponent[i,0];
			gold[i].text = gm.opponent[i,1];
			gem[i].text = gm.opponent[i,2];
		}

		Debug.Log ("LENGTH ARRAY GEM =" + gem.Length);
		for(int i=0;i<gem.Length;i++){
			string[] sortItemOpponent = new string[3];
			for(int j=0;j<gem.Length-1;j++){
				if(int.Parse(gem[j+1].text)>int.Parse(gem[j].text)){
					sortItemOpponent[0]=tim[j].text;
					sortItemOpponent[1]=gold[j].text;
					sortItemOpponent[2]=gem[j].text;

					tim[j].text=tim[j+1].text;
					gold[j].text=gold[j+1].text;
					gem[j].text=gem[j+1].text;

					tim[j+1].text=sortItemOpponent[0];
					gold[j+1].text=sortItemOpponent[1];
					gem [j+1].text = sortItemOpponent[2];

				}
			}
		}

		for (int i = 0; i < 19; i++) {
			for (int j = 0; j < 19; j++) {
				if (int.Parse(gem[i].text) > int.Parse(gem[j].text)) {
					string temp = gem [j].text;
					gem [j].text = gem [i].text;
					gem [i].text = temp;

					temp = gold [j].text;
					gold [j].text = gold [i].text;
					gold [i].text = temp;

					temp = tim [j].text;
					tim [j].text = tim [i].text;
					tim [i].text = temp;
				}
			}
		}
	}

	void checkAllowAttack(int lastAttackInSecond){
        if (lastAttackInSecond<301) {
            attackConfirmationPanel.SetActive(true);
            attackConfirmationPanelButtonList[0].SetActive(false);
            attackConfirmationPanelButtonList[1].SetActive(false);
            attackConfirmationPanelButtonList[2].SetActive(true);
            attackConfirmationText.text="You cannot attack this team yet.";
        }else if(gm.gold>=1000){ //TODO: Hardcode price
            attackConfirmationPanel.SetActive(true);
            attackConfirmationPanelButtonList[0].SetActive(true);
            attackConfirmationPanelButtonList[1].SetActive(true);
            attackConfirmationPanelButtonList[2].SetActive(false);
            attackConfirmationText.text="Are you sure you want to attack?";
        }else{
            attackConfirmationPanel.SetActive(true);
            attackConfirmationPanelButtonList[0].SetActive(false);
            attackConfirmationPanelButtonList[1].SetActive(false);
            attackConfirmationPanelButtonList[2].SetActive(true);
            attackConfirmationText.text="Your gold is insufficient for attack.";
        }
    }
	
    public void attack(int n){
        attackPanel.SetActive (false);
        int temp = 0;
        int myRank=0, enemyRank=n;
		for (int i = 0; i < gm.troops.Length; i++) {
			temp += gm.troops [i];
		}
		for (int i = 0; i < 18; i++) {
			if ((int.Parse (gm.basegem.text) < int.Parse (gem [i].text)) && (int.Parse (gm.basegem.text) > int.Parse (gem [i + 1].text))) {
				myRank = i+1;
			}
		}
		if (n >= myRank) {
			enemyRank = n + 1;
		}
		Debug.Log ("My rank "+myRank+" Enemy rank "+enemyRank);
        if (temp > 0) {
            teamToAttack=tim [n].text.ToString ();
            gm.getLastAttackedSeconds(teamToAttack,checkAllowAttack);
            /*if (gm.getLastAttackedSeconds(teamToAttack)<10) {
                attackConfirmationPanel.SetActive(true);
                attackConfirmationPanelButtonList[0].SetActive(false);
                attackConfirmationPanelButtonList[1].SetActive(false);
                attackConfirmationPanelButtonList[2].SetActive(true);
                attackConfirmationText.text="You cannot attack this team yet.";
            }else if(gm.gold>=1000){ //TODO: Hardcode price
                attackConfirmationPanel.SetActive(true);
                attackConfirmationPanelButtonList[0].SetActive(true);
                attackConfirmationPanelButtonList[1].SetActive(true);
                attackConfirmationPanelButtonList[2].SetActive(false);
                attackConfirmationText.text="Are you sure you want to attack?";
            }else{
                attackConfirmationPanel.SetActive(true);
                attackConfirmationPanelButtonList[0].SetActive(false);
                attackConfirmationPanelButtonList[1].SetActive(false);
                attackConfirmationPanelButtonList[2].SetActive(true);
                attackConfirmationText.text="Your gold is insufficient for attack.";
            }*/
        } else {
            attackConfirmationPanel.SetActive(true);
            attackConfirmationPanelButtonList[0].SetActive(false);
            attackConfirmationPanelButtonList[1].SetActive(false);
            attackConfirmationPanelButtonList[2].SetActive(true);
            attackConfirmationText.text="Your don't have enough troops.";
        }
    }

	// public void attack(int n){
	// 	attackPanel.SetActive (false);
	// 	int temp = 0;
	// 	int myRank=0, enemyRank=n;
	// 	for (int i = 0; i < gm.troops.Length; i++) {
	// 		temp += gm.troops [i];
	// 	}
	// 	for (int i = 0; i < 18; i++) {
	// 		if ((int.Parse (gm.basegem.text) < int.Parse (gem [i].text)) && (int.Parse (gm.basegem.text) > int.Parse (gem [i + 1].text))) {
	// 			myRank = i+1;
	// 		}
	// 	}
	// 	if (n >= myRank) {
	// 		enemyRank = n + 1;
	// 	}
	// 	Debug.Log ("My rank "+myRank+" Enemy rank "+enemyRank);
	// 	//tinggal kurangkan dan jadikan parameter untuk function confirm attack di bawah!!!
	// 	if (temp > 0) {
	// 		teamToAttack=tim [n].text.ToString ();
	// 		if (gm.getLastAttackedSeconds(teamToAttack)<5) {
	// 			Debug.Log("Serangan akhirnya : "+gm.getLastAttackedSeconds(teamToAttack));
	// 			attackConfirmationPanel.SetActive(true);
	// 			attackConfirmationPanelButtonList[0].SetActive(false);
	// 			attackConfirmationPanelButtonList[1].SetActive(false);
	// 			attackConfirmationPanelButtonList[2].SetActive(true);
	// 			attackConfirmationText.text="You cannot attack this team yet.";
	// 		}else if(gm.gold>=1000){ //TODO: Hardcode price
	// 			attackConfirmationPanel.SetActive(true);
	// 			attackConfirmationPanelButtonList[0].SetActive(true);
	// 			attackConfirmationPanelButtonList[1].SetActive(true);
	// 			attackConfirmationPanelButtonList[2].SetActive(false);
	// 			attackConfirmationText.text="Are you sure you want to attack?";
	// 		}else{
	// 			attackConfirmationPanel.SetActive(true);
	// 			attackConfirmationPanelButtonList[0].SetActive(false);
	// 			attackConfirmationPanelButtonList[1].SetActive(false);
	// 			attackConfirmationPanelButtonList[2].SetActive(true);
	// 			attackConfirmationText.text="Your gold is insufficient for attack.";
	// 		}
	// 	} else {
	// 		attackConfirmationPanel.SetActive(true);
	// 		attackConfirmationPanelButtonList[0].SetActive(false);
	// 		attackConfirmationPanelButtonList[1].SetActive(false);
	// 		attackConfirmationPanelButtonList[2].SetActive(true);
	// 		attackConfirmationText.text="Your don't have enough troops.";
	// 	}
	// }
	public void confirmAttack(){
		attackConfirmationPanel.SetActive(false);
		GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.gameObject.transform.position=new Vector3(5000,5000,0);
		gm.startAttack (teamToAttack);
	}
	public void cancelAttack(){
		attackConfirmationPanel.SetActive(false);
		GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.gameObject.transform.position=new Vector3(5000,5000,0);
	}

}
