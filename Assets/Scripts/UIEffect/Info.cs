﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Info : MonoBehaviour {


	public  GameObject SellPanel;

	public  GameObject UpgradePanel;

	public  GameObject RepairPanel;

	public GameObject AlertPanel;

	private GameManager gm;

	void Start(){
		gm=GameObject.Find("GameManager").GetComponent<GameManager>();
	}
		
    public void Sell(){
		transform.position=new Vector3(5000,0,0);
		SellPanel.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);
		SellPanel.GetComponent<Sell> ().costText ();
    }

	public void Upgrade(){
		AlertPanel.GetComponent<Alert> ().code = 1;
		if (int.Parse (GameObject.Find ("BuildingInfo").GetComponent<getBuildingInfo> ().level.text) >= 3) {
			transform.position=new Vector3(5000,0,0);
			AlertPanel.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);
			AlertPanel.GetComponent<Alert> ().alertText ("Max Level");
		} 
		else if (int.Parse (GameObject.Find ("BuildingInfo").GetComponent<getBuildingInfo> ().level.text) >= 2 && 
			GameObject.Find ("BuildingInfo").GetComponent<getBuildingInfo> ().namabuilding.text.ToString() == "Wall") {
			transform.position = new Vector3 (5000, 0, 0);
			AlertPanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
			AlertPanel.GetComponent<Alert> ().alertText ("Max Level");
		} 
		else {
			transform.position=new Vector3(5000,0,0);
			UpgradePanel.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);
			UpgradePanel.GetComponent<Upgrade> ().costText ();
		}
    }

    public void Repair(){
		AlertPanel.GetComponent<Alert> ().code = 1;
		if (GameObject.Find ("BuildingInfo").GetComponent<getBuildingInfo> ().curhp == GameObject.Find ("BuildingInfo").GetComponent<getBuildingInfo> ().maxhp) {
			transform.position = new Vector3 (5000, 0, 0);
			AlertPanel.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);
			AlertPanel.GetComponent<Alert> ().alertText ("Already Max HP");
		} else {
			transform.position = new Vector3 (5000, 0, 0);
			RepairPanel.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);
			RepairPanel.GetComponent<Repair> ().costText ();
		}
    }
		
	public void Exit(){
		gm.SetBuildingColliderStatus(true);
		transform.position=new Vector3(5000,0,0);
		GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.transform.position=new Vector2(5000,5000);
	}

}
