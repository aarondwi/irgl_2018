﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Alert : MonoBehaviour {
	public Text alert;
	public int code;
	public GameObject InfoPanel;
	public GameObject BarrackInfoPanel;
	public GameObject GGInfoPanel;
	// Use this for initialization
	void Start () {
		
	}
	public void alertText(string _alert){
		alert.text = _alert;
	}
	public void No(){
		gameObject.transform.position=new Vector3(2000,-1200,0);
		if (code == 1) {
			InfoPanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
		} else if (code == 3){
			GGInfoPanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
		}else {
			BarrackInfoPanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
}
