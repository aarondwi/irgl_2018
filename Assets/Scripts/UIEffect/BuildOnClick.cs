﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildOnClick : MonoBehaviour {
	public GameObject sidepanel;
	public Animator anim;
	public void Start(){
		anim = sidepanel.GetComponent<Animator> ();
	}
	public void StartBuildOnClick(){
		anim.SetBool ("BuildClicked",true);
	}



}
