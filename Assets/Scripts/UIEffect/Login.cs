﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {
	public Text t;
	public InputField username;
	public InputField password;

	// Update is called once per frame
	public void LoginGame(){
		t.gameObject.SetActive(true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
}
