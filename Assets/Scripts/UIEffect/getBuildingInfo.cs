﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class getBuildingInfo : MonoBehaviour {
	
	public Text namabuilding;
	public Text level;
	public Text damage;
	public Text hp;
	public float maxhp;
	public float curhp;
	public Text types;
	public Text range;
	public Text atkspeed;
	public Text sell;
	public Text upgrade;
	public Text repair;
	public Image buildingimage;
	public Sprite spriteArcher;
	public Sprite spriteCanon;
	public Sprite spriteTesla;
	public Sprite spriteWall;
	[HideInInspector]public string namatower;

	public void setAll(string _namabuilding, string _level, string _damage, string _hp, float _maxhp,string _types, string _range, string _atkspeed, string _sell, string _upgrade, string _repair, string _namatower){
		namabuilding.text = _namabuilding;
		// level.text = "Lv "+_level;
		level.text = _level;
		damage.text = _damage;
		hp.text = _hp+"/"+_maxhp;
		curhp = float.Parse (_hp);
		maxhp = _maxhp;
		types.text = _types;
		range.text = _range;
		atkspeed.text = _atkspeed;
		sell.text = _sell;
		upgrade.text = _upgrade;
		repair.text = _repair;
		namatower = _namatower;
		if (_namabuilding == "Canon") {
			buildingimage.sprite = spriteCanon;
		}
		else if (_namabuilding == "Archer") {
			buildingimage.sprite = spriteArcher;
		}
		else if (_namabuilding == "Mage") {
			buildingimage.sprite = spriteTesla;
		}
		else if (_namabuilding == "Wall") {
			buildingimage.sprite = spriteWall;
		}
	}
}
