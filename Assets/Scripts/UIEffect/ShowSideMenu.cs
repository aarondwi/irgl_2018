﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowSideMenu : MonoBehaviour {

	public GameObject sidepanel;
	public Animator anim;
	public GameObject attackpanel;
	public bool sidestate=false;
	public bool buildstate = false;

	void Start(){
		anim = sidepanel.GetComponent<Animator> ();
	}

	public void StartAttack(){
		attackpanel.SetActive (true);
		GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);
		CloseSide ();
	}

	public void Exit(){
		attackpanel.SetActive (false);
		GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.gameObject.transform.position=new Vector3(5000,5000,0);
	}

	public void StartBuild(){
		anim.SetBool ("BuildClicked",true);
		buildstate = true;
		sidestate = false;
	}

	public void ShowSide(){
		sidepanel.gameObject.SetActive (true);
		anim.SetBool ("ShowMenu",true);
		this.gameObject.SetActive (false);
		buildstate = false;
		sidestate = true;
	}
	public void CloseSide(){
		if (sidestate == true) {
			anim.SetBool ("ShowMenu", false);
		}
		if (buildstate == true) {
			anim.SetBool ("BuildClicked", false);
			anim.SetBool ("ShowMenu", false);
		}
		this.gameObject.SetActive (true);
	}
}
