﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarrackInfo : MonoBehaviour {
	public  GameObject TroopsPanel;

	public  GameObject UpgradePanel;

	public  GameObject RepairPanel;

	public GameObject AlertPanel;

	public Text nama, hp, capacity, repair, upgrade, level;
	public float maxhp;
	public float curhp;
	public GameObject attackConfirmationPanel;
	public Text attackConfirmationText;
	public string stringCapacity;
	private GameManager gm;

	public string namatower="tower1";
	void Start(){
		gm=GameObject.Find("GameManager").GetComponent<GameManager>();
	}

	public void setCapacity(int temp){
		capacity.text = temp.ToString()+"/"+stringCapacity;
	}

	public void setAll(string _nama, string _level, string _hp, float _maxhp,string _capacity, string _upgrade, string _repair){
		repair.text = _repair;
		//level.text = "Lv "+_level;
		level.text = _level;
		upgrade.text = _upgrade;
		nama.text = _nama;
		curhp = float.Parse (_hp);
		maxhp = _maxhp;
		hp.text =  _hp+"/"+_maxhp;
		int temp = 0;
		for (int i = 0; i < gm.troops.Length; i++) {
			temp += gm.troops [i]*gm.CapacityTroop[i];
		}
		stringCapacity = _capacity;
		capacity.text = temp.ToString()+"/"+_capacity;
		AlertPanel.GetComponent<Alert> ().code = 2;
		namatower = "tower1";
	}

	public void setggAll(string _nama, string _level, string _hp, float _maxhp,string _capacity, string _upgrade, string _repair){
		repair.text = _repair;
		//level.text = "Lv "+_level;
		level.text = _level;
		upgrade.text = _upgrade;
		nama.text = _nama;
		maxhp = _maxhp;
		hp.text = _hp;
		AlertPanel.GetComponent<Alert> ().code = 3;
		if (_nama == "Gem Reactor") {
			namatower = "tower2";
		} else {
			namatower = "tower3";
		}
	}

	public void Troops(){
		if (curhp > 0) {
			this.gameObject.SetActive (false);
			TroopsPanel.GetComponent<Troop> ().capacityText.text = "Capacity: "+capacity.text;
			TroopsPanel.SetActive (true);
		} else {
			attackConfirmationPanel.SetActive(true);
			attackConfirmationText.text="Please repair your barrack.";
		}
	}

	public void Upgrade(){
		if (int.Parse (level.text) >= 3) {
			transform.position = new Vector3 (5000, 0, 0);
			AlertPanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
			AlertPanel.GetComponent<Alert> ().alertText ("Max Level");
		} 
		else {
			this.gameObject.transform.position = new Vector3 (5000, 0, 0);
			UpgradePanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
			UpgradePanel.GetComponent<Upgrade> ().costText ();
		}
	}

	public void Repair(){
		if (float.Parse (hp.text) == maxhp) {
			transform.position = new Vector3 (5000, 0, 0);
			AlertPanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
			AlertPanel.GetComponent<Alert> ().alertText ("Already Max HP");
		} else {
			this.gameObject.transform.position = new Vector3 (5000, 0, 0);
			RepairPanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
			RepairPanel.GetComponent<Repair> ().costText ();
		}
	}

	public void Exit(){
		gm.SetBuildingColliderStatus(true);
		transform.position=new Vector3(5000,0,0);
		GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.transform.position=new Vector3(5000,0,0);
	}
	public void cancelBuy(){
		attackConfirmationPanel.SetActive(false);
	}

}
