﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TroopsController : MonoBehaviour {

	/* PRIVATE VARIABLE AREA */
	private Rigidbody rb;
	private GameManager gm;
	private BoxCollider bc;
	private bool isDestroyed = false;
	private Animator anim;
	private float animDeadDuration = 0.2f;
	private float timerAttack;
	private static int wallLayerMask = 1 << 8;
	private static float rayCastYAxisOrigin = 5;
	private static Color hitColor=new Color(0.898f, 0.224f, 0.208f, 1f);
	private static float hitDuration=0.25f;
	//private List<Color> originalColor;
	private static Color originalColor=new Color(1f, 1f, 1f, 1f);


	/* PUBLIC VARIABLE AREA */
	public float totalMoveSpeed;
	public float atkRange;
	public float throwableAttackNormal;
	public float throwableAttackWall;
	public float hp;
	public float prefabAngleRotator;
	public bool isFlyingUnit;//untuk type
	public GameObject throwObject;
	public Transform throwingPoint;
	public float thrust;
	public SkinnedMeshRenderer[] hitIndicator;

	/* HIDEININSPECTOR VARIABLE AREA */
	[HideInInspector] public GameObject target;
	[HideInInspector] public Vector3 moveSpeed;
	[HideInInspector] public Vector3 startingAttackPos;
	[HideInInspector] public float timerDead=0f;
	[HideInInspector] public Vector3 dirVector;

	/* HIDEININSPECTOR STATE VARIABLE AREA */
	[HideInInspector] public bool xMatched = false;
	[HideInInspector] public bool zMatched = false;


	void Start () {
		totalMoveSpeed=totalMoveSpeed/transform.localScale.x;
		rb = GetComponent<Rigidbody> ();
		bc=GetComponent<BoxCollider>();
		anim = GetComponent<Animator> ();
		gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		//foreach(SkinnedMeshRenderer smr in hitIndicator) originalColor.Add(smr.material.color);
	//	StartCoroutine ("SearchNewTarget");
	}

	public void setUpTroops(float _attackNormal, float _attackWall, float _hp, float _movespeed, float _attackrange){
		Debug.Log (_attackNormal + "troopsattack"+_attackWall);
		hp = _hp;
		//thrust = _attack;
		throwableAttackNormal=_attackNormal;
		throwableAttackWall = _attackWall;
		atkRange = _attackrange;
		totalMoveSpeed = _movespeed;
		StartCoroutine ("SearchNewTarget");
	}

	void Update () {
		if (isDestroyed) {
			timerDead += Time.deltaTime;
			if (timerDead > animDeadDuration) {
				Destroy (gameObject);
			}
		}
		if (target) {
			if (!xMatched || !zMatched) {

				rb.velocity = moveSpeed;

				if (((float)Mathf.Round (transform.position.x / 2)) * 2 == ((float)Mathf.Round (startingAttackPos.x / 2)) * 2) {
					xMatched = true;
					moveSpeed = new Vector3 (0, moveSpeed.y, moveSpeed.z);
					//rb.velocity = new Vector3 (0, rb.velocity.y, rb.velocity.z);
				}
				if (((float)Mathf.Round (transform.position.z / 2)) * 2 == ((float)Mathf.Round (startingAttackPos.z / 2)) * 2) {
					zMatched = true;
					moveSpeed = new Vector3 (moveSpeed.x, moveSpeed.y, 0);
					//rb.velocity = new Vector3 (rb.velocity.x, rb.velocity.y, 0);
				}

				if (xMatched && zMatched) {
					anim.SetBool ("isRunning", false);
					anim.SetBool ("isAttacking", true);
					rb.constraints=RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
				}
			}
		} else {
			xMatched = false;
			zMatched = false;
			anim.SetBool ("isAttacking", false);
			StartCoroutine ("SearchNewTarget");
		}
	}

	IEnumerator SearchNewTarget(){
		yield return new WaitForSeconds(0.1f);
		//determine target building excluding wall
		target = gm.DetermineTroopsTarget(transform.position);
		if(target){ // if not null, check with raycast if some wall is on troops' way
			//rotate to face the target
			float xDistEarlyTarget = target.transform.position.x - transform.position.x;
			float zDistEarlyTarget = target.transform.position.z - transform.position.z;
			float dirDegreeEarlyTarget = xDistEarlyTarget < 0 ? 
											180f - prefabAngleRotator - Mathf.Atan (zDistEarlyTarget / xDistEarlyTarget) * 180 / Mathf.PI :
											360f - prefabAngleRotator - Mathf.Atan (zDistEarlyTarget / xDistEarlyTarget) * 180 / Mathf.PI;
			transform.localEulerAngles = new Vector3 (0, dirDegreeEarlyTarget % 360, 0);
			
			//do raycast on center, right, left of troops
			float raycastRange=gm.CalculateDistance (transform.position, target.transform.position);
			RaycastHit wallHitCenter, wallHitRight, wallHitLeft;
			float distHitCenter=Mathf.Infinity, distHitRight=Mathf.Infinity, distHitLeft=Mathf.Infinity;
			Vector3 leftEdge=bc.bounds.center+transform.TransformDirection(bc.bounds.extents-new Vector3(2*bc.bounds.extents.x,bc.bounds.extents.y,bc.bounds.extents.z));
			Vector3 rightEdge=bc.bounds.center+transform.TransformDirection(bc.bounds.extents-new Vector3(0,bc.bounds.extents.y,bc.bounds.extents.z));
			if (Physics.Raycast(new Vector3(bc.bounds.center.x,rayCastYAxisOrigin,bc.bounds.center.z),
						transform.TransformDirection(Vector3.forward), out wallHitCenter, raycastRange, wallLayerMask)){
				//raycast from center of object
				Debug.Log("Center Hit");
				distHitCenter=gm.CalculateDistance (transform.position, wallHitCenter.collider.gameObject.transform.position);
			}
			if (Physics.Raycast(new Vector3(rightEdge.x,rayCastYAxisOrigin,rightEdge.z),
						transform.TransformDirection(Vector3.forward), out wallHitRight, raycastRange, wallLayerMask)){
				//raycast from right edge of its collider
				Debug.Log("Right Hit");
				distHitRight=gm.CalculateDistance (transform.position, wallHitRight.collider.gameObject.transform.position);
			}
			if (Physics.Raycast(new Vector3(leftEdge.x,rayCastYAxisOrigin,leftEdge.z),
						transform.TransformDirection(Vector3.forward), out wallHitLeft, raycastRange, wallLayerMask)){
				//raycast from left edge of its collider
				Debug.Log("Left Hit");
				distHitLeft=gm.CalculateDistance (transform.position, wallHitLeft.collider.gameObject.transform.position);
			}

			//choose the nearest hit object
			float minHitDistance=Mathf.Min(new float[]{distHitCenter,distHitRight,distHitLeft});
			if(minHitDistance!=Mathf.Infinity){
				if(minHitDistance==distHitCenter){
					target=wallHitCenter.collider.gameObject;
				}else if(minHitDistance==distHitRight){
					target=wallHitRight.collider.gameObject;
				}else{
					target=wallHitLeft.collider.gameObject;
				}
			}
		}else{
			target = gm.DetermineTroopsTarget(transform.position,true);
		}
		
		if (target) {
			float xDist = target.transform.position.x - transform.position.x;
			float zDist = target.transform.position.z - transform.position.z;
			float totalDistance = Mathf.Sqrt (Mathf.Pow (xDist, 2) + Mathf.Pow (zDist, 2));

			if (totalDistance > atkRange) {
				float ratio = atkRange / totalDistance; //sblmnya pakai target distance
				startingAttackPos = new Vector3 (target.transform.position.x - (ratio * xDist), target.transform.position.y, target.transform.position.z - (ratio * zDist));
				moveSpeed = new Vector3 (xDist / (Mathf.Abs (xDist) + Mathf.Abs (zDist)) * totalMoveSpeed, 0, zDist / (Mathf.Abs (xDist) + Mathf.Abs (zDist)) * totalMoveSpeed);
			} else {
				startingAttackPos = transform.position;
				moveSpeed = new Vector3 (0, 0, 0);
				//TODO: kalau pakai cara ini, jalan loop !xmatch !ymatch 1x dahulu baru berhenti. Apakah berbahaya?
			}

			float dirDegree = xDist < 0 ? 180f - prefabAngleRotator - Mathf.Atan (zDist / xDist) * 180 / Mathf.PI : 360f - prefabAngleRotator - Mathf.Atan (zDist / xDist) * 180 / Mathf.PI;
			transform.localEulerAngles = new Vector3 (0, dirDegree % 360, 0);

			rb.constraints=RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			anim.SetBool ("isRunning", true);
		}
		StopCoroutine("SearchNewTarget");
	}

	public void Hit(float dmg){
		hp -= dmg;
		//flash red
		foreach(SkinnedMeshRenderer smr in hitIndicator) smr.material.color = hitColor;
		Invoke("ResetColor", hitDuration);

		if (hp <= 0) {
			anim.SetBool ("isRunning", false);
			anim.SetBool ("isAttacking", false);
			anim.SetTrigger ("isDead");
			isDestroyed = true;
		}
	}

	public void ResetColor(){
		for(int i=0; i<hitIndicator.Length; i++){
			//hitIndicator[i].material.color = originalColor[i];
			hitIndicator[i].material.color = originalColor;
		}
	}

	public void FireThrowObject(){
		if (target) {
			GameObject throwObjectTemp = (GameObject)Instantiate (throwObject, throwingPoint.position, Quaternion.Euler (new Vector3 (throwObject.transform.rotation.x, transform.rotation.y, 0)));
			Vector3 downVector = new Vector3 (0, throwingPoint.position.y / CalculateDistance (), 0);
			DetermineDirVector ();
			if (throwObjectTemp.name.Contains("Arrow")) {
				throwObjectTemp.transform.eulerAngles = new Vector3 (
				throwObjectTemp.transform.eulerAngles.x,
				throwObjectTemp.transform.eulerAngles.y+this.transform.eulerAngles.y-90,
				throwObjectTemp.transform.eulerAngles.z+90
				);
			}
			TroopsThrowController ttc = throwObjectTemp.GetComponent<TroopsThrowController> ();
			ttc.throwDmgNormal = throwableAttackNormal;
			ttc.throwDmgWall = throwableAttackWall;
			throwObjectTemp.GetComponent<Rigidbody> ().AddForce ((dirVector - downVector) * thrust, ForceMode.Impulse);
		}
	}

	float CalculateDistance(){
		float xDist = target.transform.position.x - throwingPoint.position.x;
		float zDist = target.transform.position.z - throwingPoint.position.z;
		return Mathf.Sqrt (Mathf.Pow (xDist, 2) + Mathf.Pow (zDist, 2));
	}
	void DetermineDirVector(){
		if (prefabAngleRotator == 0f) {
			dirVector = transform.right;
		} else if (prefabAngleRotator == 270f) {
			dirVector = transform.forward;
		} else if (prefabAngleRotator == 180f) {
			dirVector = -transform.right;
		} else {
			dirVector = -transform.forward;
		}
	}

	void OnCollisionEnter(Collision other){
		if(other.gameObject.tag=="Troops"){
			Physics.IgnoreCollision(other.gameObject.GetComponent<BoxCollider>(),bc);
		}
	}
}
