﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerController : MonoBehaviour {
	public string namabuilding;
	public string types;
	/* PRIVATE VARIABLE AREA */
	private float timerAttack=0f;
	private bool isPressed = false;
	//private cakeslice.OutlineEffect oe;
	private DBManager DBm;
	private GameManager gm;
	private CapsuleCollider cc;
	//private cakeslice.Outline[] outlineArr;
	public GameObject buildingInfoPanel;
	private GameObject barrackInfoPanel;
	private GameObject ggInfoPanel;
	private DigitalRuby.LightningBolt.LightningBoltScript lb;
	private float teslaDamage;
	private bool isDestroyed;
	private bool shouldBeSubtituted=false;
	private int snappingFactor=2;
	public int[] capacity = new int[3]{100,150,200};
	public static Color[] levelColor={
		new Color(0.62f, 0.73f, 0.486f, 1f),//green
		new Color(1f, 0.922f, 0.231f, 1f),//yellow
		new Color(0.898f, 0.224f, 0.208f, 1f)//red
		};
	private static Color noColor=new Color(1f, 1f, 1f, 0f);
	private static Color placementColor=new Color(0f, 1f, 0f, 1f);
	private static Color rejectedColor=new Color(1f, 0f, 0f, 1f);

	/* PUBLIC VARIABLE AREA */
	public GameObject[] buildingParts;
	public MeshRenderer[] buildingPartsRenderer;
	public GameObject ruin;
	public GameObject ruinEffect;
	public Transform ruinEffectStartingPoint;
	public bool isNotThrowable;
	public bool isNotTower;
	public GameObject throwObject;
	public Transform throwingPoint;
	public GameObject particleEffect;
	public GameObject towerRange;
	public float attackSpeed;
	public float thrust;
	public float prefabAngleRotator;
	public int towerAttackType;//0=land 1=all
	public float hp;
	public float hpMax;
	public float damage;
	public int arah;
	public int buildingCode;
	public float throwableAttack;
	public Transform rotatingPart;
	public float ccBaseRadius;
	public float ccAttackRadius;
	public float ccBaseHeight;
	public float teslaBaseDamage;
	public float teslaMultiplier;
	public string namatower;
	public GameObject groundPlane;
	public MeshRenderer[] towerLevelIndicator;


	/* HIDEININSPECTOR VARIABLE AREA */
	[HideInInspector] public int towerLevel;
	
	[HideInInspector] public GameObject target;
	[HideInInspector] public ArrayList targetList = new ArrayList();
	[HideInInspector] public float rotateDegree;
	[HideInInspector] public float targetDegree;
	[HideInInspector] public Vector3 dirVector;
	[HideInInspector] public GameObject guiPlacement;
	[HideInInspector] public Plane projectedPlane;


	/* HIDEININSPECTOR STATE VARIABLE AREA*/
	[HideInInspector] public bool isRotating=false;
	[HideInInspector] public bool isPlacementDone;
	[HideInInspector] public bool isPlacementPermitted = true;//defaultnya apa bisa true? krn hrsnya checkernya akan langsung mengubah agar false jika memang numpuk
	[HideInInspector] public bool spawnFromDB;

	void Start () {
		gm=GameObject.Find("GameManager").GetComponent<GameManager>();
		DBm=GameObject.Find("DBManager").GetComponent<DBManager>();
		//oe=GameObject.Find("MainCamera").GetComponent<cakeslice.OutlineEffect>();
		teslaDamage = teslaBaseDamage;
		ColorizeBuilding(towerLevelIndicator,towerLevel!=null?levelColor[towerLevel]:levelColor[0]);
		if (gm.gameplayMode == 0 && spawnFromDB == false) { //ifnya kurang elegan semua kecuali guiplacement&reposition bisa jd 1 tnp di if
			//SetupTower(
			isPlacementDone = false;
			ColorizeBuilding(buildingPartsRenderer,placementColor);
			//outlineArr = GetComponentsInChildren<cakeslice.Outline> ();
			// foreach (cakeslice.Outline outline in outlineArr) {
			// 	outline.enabled = true;
			// }
			cc = GetComponent<CapsuleCollider> ();
			cc.radius = ccBaseRadius;
			if (towerRange!=null) {//wall tidak punya towerrange. perlu diganti dengan cara lain
				towerRange.SetActive(true);
				towerRange.transform.localScale=new Vector3(2*ccAttackRadius,0.005f,2*ccAttackRadius);
			}
			guiPlacement = GameObject.Find ("PlacementButtons");
			RepositionPlacementButtons ();
		} else {
			isPlacementDone = true;
			ColorizeBuilding(buildingPartsRenderer,noColor);
			//outlineArr = GetComponentsInChildren<cakeslice.Outline> ();
			// foreach (cakeslice.Outline outline in outlineArr) {
			// 	outline.enabled = false;
			// }
			if (gm.gameplayMode == 0) {
				cc = GetComponent<CapsuleCollider> ();
				cc.radius = ccBaseRadius;
				cc.height = ccBaseHeight;
				if(towerRange!=null){
					towerRange.SetActive(true);
					towerRange.transform.localScale=new Vector3(2*ccAttackRadius,0.005f,2*ccAttackRadius);
				}
				transform.GetChild (transform.childCount - 1).GetComponent<BoxCollider> ().enabled = true;
				if(hp<=0){
					SwapBuildingAndRuin(false);
					gm.buildingList.Remove(gameObject);
					isDestroyed=true;
				}
			} else {
				if (!isNotTower) {
					cc = GetComponent<CapsuleCollider> ();
					cc.radius = ccAttackRadius;
				} else {
					GetComponent<CapsuleCollider> ().enabled = false;
				}
				if (isNotThrowable) {
					lb = throwObject.GetComponent<DigitalRuby.LightningBolt.LightningBoltScript> ();
				}
				if(hp<=0){
					shouldBeSubtituted=false;
					if (isNotThrowable) throwObject.SetActive (false);
					SwapBuildingAndRuin(false);
					if(cc) cc.enabled=false;
					gm.buildingList.Remove(gameObject);
					isDestroyed=true;
				}
			}
		}
	}

	void Update () {
		if (isPlacementDone) {
			if(shouldBeSubtituted){
				/* Jika GoldTower atau GemTower */
				if(buildingCode==5 || buildingCode==6) gm.reactorAmount--;
				gm.HPBarTower [gm.buildingList.IndexOf(gameObject)].gameObject.SetActive(false);
				//gm.buildingList.Remove(gameObject);
				isDestroyed=true;
				Instantiate (ruinEffect, ruinEffectStartingPoint.position, Quaternion.identity);
				
				GameObject ruinSubtitute=(GameObject) Instantiate (gameObject, transform.position, Quaternion.identity);
				Destroy(gameObject);
			}
			if (isDestroyed) {
				//Destroy (gameObject);
			}else{
				if (!isNotTower) {
					if (target) {
						if (isRotating) {
							isRotating = (int)(rotatingPart.rotation.eulerAngles.y / 5) == (int)(targetDegree / 5) ? false : isRotating;
							if(isRotating){
								rotatingPart.RotateAround (rotatingPart.position, Vector3.up, rotateDegree * Time.deltaTime);
							}
						}
						if (!isRotating) {
							timerAttack += Time.deltaTime;
							SetTowerRotation();
						}
						if (timerAttack >= attackSpeed) {
							if (!isNotThrowable) {
								GameObject throwObjectTemp = (GameObject)Instantiate (throwObject, throwingPoint.position, throwObject.transform.rotation);
								if(particleEffect) Instantiate (particleEffect, throwingPoint.position, Quaternion.identity);
								Vector3 downVector = new Vector3 (0, (throwingPoint.position.y-target.transform.position.y-target.transform.localScale.y) / CalculateDistance (), 0);
								if (buildingCode == 2) {
									float teta = Mathf.Atan((throwingPoint.position.y-target.transform.position.y-target.transform.localScale.y) / CalculateDistance ());
									teta = Mathf.Rad2Deg*teta;
									throwObjectTemp.transform.eulerAngles = new Vector3 (
									-90+teta,
									throwingPoint.transform.eulerAngles.y+180,
									0
									);
								}
								DetermineDirVector ();
								throwObjectTemp.GetComponent<Rigidbody> ().AddForce ((dirVector - downVector) * thrust, ForceMode.Impulse);
							} else {
								target.GetComponent<TroopsController> ().Hit (teslaDamage);
								teslaDamage *= teslaMultiplier;
							}
							timerAttack = 0f;
						}
					} else if (targetList.Count > 0 && !target) {
						if (isNotThrowable) {
							throwObject.SetActive (false);
							teslaDamage = teslaBaseDamage;
						}
						while (!target && targetList.Count > 0) {
							if (targetList [0] != null) {
								target = (GameObject)targetList [0];
								targetList.RemoveAt (0);
								SetTowerRotation ();//FIXME: kenapa bisa msk sini?
							} else {
								targetList.RemoveAt (0);
							}
						}
					} else {
						if (isNotThrowable) {
							throwObject.SetActive (false);
							teslaDamage = teslaBaseDamage;
						}
					}
				} else {//elsenya if isnottower, untuk yang berupa wall, barrak, castle
					//kelihatannya tidak akan perlu else
				}
			}
		} else {
			CheckPressed ();
		}
	}

	void OnTriggerEnter(Collider other) {
		if (!isNotTower) {
			if (other.gameObject.tag == "Troops") {
				//jika tipe serang tower cuma ada 2 (darat dan darat+udara)
				if (towerAttackType == 0 && other.gameObject.transform.position.y <= 5f || towerAttackType >= 1) {
					if (!target && targetList.Count == 0) {
						target = other.gameObject;
						SetTowerRotation ();
					} else {
						targetList.Add (other.gameObject);
					}
				}
			}
		}
	}

	void OnTriggerExit(Collider other){
        if (!isNotTower) {
			if (other.gameObject.tag == "Troops") {
				if (towerAttackType == 0 && other.gameObject.transform.position.y <= 5f || towerAttackType >= 1) {
					if (other.gameObject == target) {
						target=null;
					}else{
						targetList.Remove(other.gameObject);
					}
				}
			}
		}
    }

	void SetTowerRotation(){
		float xDist = target.transform.position.x - rotatingPart.position.x;
		float zDist = target.transform.position.z - rotatingPart.position.z;
		targetDegree = xDist<0 ? 180f-prefabAngleRotator-Mathf.Atan (zDist / xDist) * 180/Mathf.PI : 360f-prefabAngleRotator-Mathf.Atan (zDist / xDist) * 180/Mathf.PI;
		rotateDegree = ((targetDegree % 360) - rotatingPart.rotation.y);
		targetDegree %= 360;
		targetDegree = targetDegree < 0 ? targetDegree + 360 : targetDegree;
		isRotating=true;
		if (isNotThrowable) {
			lb.EndObject = target;
			throwObject.SetActive (true);
		}
	}

	float CalculateDistance(){
		float xDist = target.transform.position.x - throwingPoint.position.x;
		float zDist = target.transform.position.z - throwingPoint.position.z;
		return Mathf.Sqrt (Mathf.Pow (xDist, 2) + Mathf.Pow (zDist, 2));
	}
	void DetermineDirVector(){
		if (prefabAngleRotator == 0f) {
			dirVector = rotatingPart.right;
		} else if (prefabAngleRotator == 270f) {
			dirVector = rotatingPart.forward;
		} else if (prefabAngleRotator == 180f) {
			dirVector = -rotatingPart.right;
		} else {
			// CARA AGAR TIDAK PERLU DI IF KHUSUS UNTUK WATCHTOWER BAGAIMANA????
			//dirVector = -rotatingPart.forward;
			dirVector= buildingCode==2 ? rotatingPart.up : -rotatingPart.forward;
		}
	}
	void OnMouseDown(){
		if (!isPlacementDone) {
			isPressed = true;
			gm.isBuildingPressed = isPressed;
		}
	}
	void OnMouseUp(){
		if (!isPlacementDone) {
			isPressed = false;
			gm.isBuildingPressed = isPressed;
		} else {
			if(gm.gameplayMode==0 && !gm.attackPanel.activeSelf){
				
				GameObject.Find ("GameManager").GetComponent<GameManager> ().hiddenButton.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);

				ccBaseRadius = DBm.Range[buildingCode, towerLevel];
				if (buildingCode < 4) {
					if (!buildingInfoPanel) {
						buildingInfoPanel = GameObject.Find ("BuildingInfo");
					}
					namabuilding = gm.listNamaBuilding [buildingCode];
					if (towerAttackType == 0) {
						types = "Ground";
					} else if (towerAttackType >= 1) {
						types = "Air and Ground";
					}

					buildingInfoPanel.GetComponent<getBuildingInfo>().setAll(
						namabuilding,
						(towerLevel+1).ToString(),
						DBm.Attack[buildingCode, towerLevel].ToString(),
						hp.ToString(),
						DBm.HP[buildingCode, towerLevel],
						types,
						ccBaseRadius.ToString(),
						DBm.AttackSpeed[buildingCode, towerLevel].ToString(),
						DBm.sell[buildingCode, towerLevel].ToString(),
						DBm.upgrade[buildingCode, towerLevel].ToString(),
						DBm.repair[buildingCode, towerLevel].ToString()
						,namatower);
					buildingInfoPanel.GetComponent<RectTransform>().anchoredPosition=new Vector2(0,0);//tidak elegan new vector tiap kali klik
					//buildingInfoPanel.transform.position=Camera.main.WorldToScreenPoint(new Vector3(0,0,0));
					gm.SetBuildingColliderStatus (false);
				} else if (buildingCode == 4) {
					if (!barrackInfoPanel) {
						barrackInfoPanel = GameObject.Find ("BarrackInfo");
					}
					namabuilding = gm.listNamaBuilding [buildingCode];
					barrackInfoPanel.GetComponent<BarrackInfo> ().setAll (
						namabuilding,
						(towerLevel+1).ToString(),
						hp.ToString(),
						DBm.HP[buildingCode, towerLevel],
						gm.capacity[towerLevel].ToString(),
						DBm.upgrade[buildingCode, towerLevel].ToString(),
						DBm.repair[buildingCode, towerLevel].ToString()
					);
					barrackInfoPanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
					//barrackInfoPanel.transform.position=Camera.main.WorldToScreenPoint(new Vector3(0,0,0));
					gm.SetBuildingColliderStatus (false);
				} else {
					if (!ggInfoPanel) {
						ggInfoPanel = GameObject.Find ("GGInfo");
					}
					namabuilding = gm.listNamaBuilding [buildingCode];
					ggInfoPanel.GetComponent<BarrackInfo> ().setggAll (
						namabuilding,
						(towerLevel + 1).ToString (),
						hp.ToString (),
						hpMax,
						"0",
						DBm.upgrade [buildingCode, towerLevel].ToString (),
						DBm.repair [buildingCode, towerLevel].ToString ()
					);
					ggInfoPanel.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
					//barrackInfoPanel.transform.position=Camera.main.WorldToScreenPoint(new Vector3(0,0,0));
					gm.SetBuildingColliderStatus (false);
				}
			}
		}
	}
	void CheckPressed(){
		if (isPressed) {
			if (Time.frameCount % 3 == 0) {
				Vector3 objPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				groundPlane=GameObject.Find("PlayPlane");
				projectedPlane = new Plane (Vector3.up, groundPlane.transform.position);
				Ray touchRay = Camera.main.ScreenPointToRay (Input.mousePosition);
				float rayDistance;
				if (projectedPlane.Raycast (touchRay, out rayDistance)) {
					Vector3 tempBuildingPos = touchRay.GetPoint (rayDistance);
					transform.position = new Vector3(Mathf.Round(tempBuildingPos.x/snappingFactor)*snappingFactor, tempBuildingPos.y, Mathf.Round(tempBuildingPos.z/snappingFactor)*snappingFactor);
					RepositionPlacementButtons ();
				}
			}
		}
	}

	public void ModifyPlacementPermission(bool valueChange){
		isPlacementPermitted = valueChange;
		if (isPlacementPermitted) {
			ColorizeBuilding(buildingPartsRenderer,placementColor);
			//oe.lineColor0 = new Color (0.289f, 1f, 0.243f);
		} else {
			ColorizeBuilding(buildingPartsRenderer,rejectedColor);
			//oe.lineColor0 = new Color (1f, 0f, 0.133f);
		}
	}

	public void RepositionPlacementButtons(){
		if (isNotTower) {
			guiPlacement.transform.Find ("PlacementRotate").gameObject.SetActive (true);
		} else {
			guiPlacement.transform.Find ("PlacementRotate").gameObject.SetActive (false);
		}
		guiPlacement.transform.position = Camera.main.WorldToScreenPoint(transform.position);
	}
	public void ConfirmPlacement(bool isPlacementAccepted){
		if (isPlacementAccepted) {
			if (isPlacementPermitted) {
				isPlacementDone = true;
				guiPlacement.transform.position=new Vector2(5000,5000);
				// foreach (cakeslice.Outline outline in outlineArr) {
				// 	outline.enabled = false;
				// }
				// if (buildingCode < 3) {
				// 	towerRange.SetActive(false);
				// }
				ColorizeBuilding(buildingPartsRenderer,noColor);
				namatower = "tower" + (gm.buildingNumber + 1);
				hp = DBm.HP [buildingCode, 0];
				gm.AddBuildingToList (gameObject);
				gm.FinishPlacement(true,(int)Mathf.Ceil(transform.rotation.y),buildingCode,(int)transform.position.x,(int)transform.position.z);
			}
		} else {
			guiPlacement.transform.position=new Vector2(5000,5000);
			gm.FinishPlacement(false,(int)Mathf.Ceil(transform.rotation.y),buildingCode,0,0);
			Destroy (gameObject);
		}

	}

	public void Hit(float dmg){
		if(dmg>hp) dmg=hp;
		hp -= dmg;
		hp=Mathf.Max(hp,0f);
		gm.updateHP (dmg);
		gm.HPBarTower [gm.buildingList.IndexOf(gameObject)].value = (float)(hp / hpMax);
		if (hp <= 0) {
			shouldBeSubtituted=true;
		}
	}

	public void SwapBuildingAndRuin(bool shouldBuildingActive){
		foreach(GameObject bp in buildingParts) bp.SetActive(shouldBuildingActive);
		ruin.SetActive(!shouldBuildingActive);
	}

	public static void ColorizeBuilding(MeshRenderer[] partsToColorize, Color color){
		// Debug.Log(partsToColorize.Length);
		foreach(MeshRenderer mr in partsToColorize) mr.material.color = color;
	}

	public void RotateBuilding(){
		transform.RotateAround (transform.position, Vector3.up, 90);
	}

	public void SetupTower(bool _spawnFromDB, int _buildingCode, int _towerLevel, float _attack,float _hpMax, float _hp, float _attackSpeed, int _arah, string _namatower){
		spawnFromDB = _spawnFromDB;
		buildingCode = _buildingCode;
		towerLevel = _towerLevel;
		throwableAttack = _attack;
		hpMax = _hpMax;
		hp = _hp;
		attackSpeed = _attackSpeed;
		namatower = _namatower;
		arah = _arah;
		//cc.radius = DBm.Range [buildingCode, towerLevel];
		transform.RotateAround (transform.position, Vector3.up, 90*_arah);
	}
}
