﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour {

	public GameObject[] buildingPrefabsList;
	public GameObject[] troopsPrefabsList;
	public Slider HPBar;
	[HideInInspector]public List<Slider> HPBarTower;
	[HideInInspector]public Slider newHPBarTroop;
	[HideInInspector]public Slider[] HPBarTroop;
	public int gameplayMode; //0=base 1=attack
	public GameObject groundPlane;
	public GameObject attackPanel;
	public float enemyhp;
	public float currentenemyhp;
	public float[] defenderAreaLimit; //xmin, xmax, zmin, zmax
	public GameObject resultpanel;
	public GameObject resultpanel2;
	public Text basegold;
	public Text basegem;
	public Text baseteam;
	private int enemyGold;
	private int enemyGem;
	private int myGold;
	private int myGem;
	public Text myTeamText;
	public Text enemyTeamText;
	public Text enemyGoldText;
	public Text enemyGemText;
	public Text myGoldText;
	public Text myGemText;
	public Text time;
	public int[] troops = new int[6];
	public Text[] pricetexttroops = new Text[6];
	public Text[] basetexttroops = new Text[6];
	public Text[] capacitytexttroops = new Text[6];
	public Text[] texttroops = new Text[6];

	public float targetTime;//TODO: pertimbangkan ulang kecukupan waktunya

	public GameObject placementRotate;
	public GameObject underAttackScene;
	public GameObject hiddenButton;
	[HideInInspector] public List<GameObject> buildingList;
	[HideInInspector] public List<GameObject> originalBuildingList;
	[HideInInspector] public List<GameObject> enemyBuildingList;
	[HideInInspector] public TowerController currentBuildingPlacement;
	[HideInInspector] public bool isPlacementDone;
	[HideInInspector] public bool isBuildingPressed=false;
	[HideInInspector] public int currentSelectedTroopsPrefab=-1;
	[HideInInspector] public Plane projectedPlane;

	[HideInInspector] public int gold;
	[HideInInspector] public int gem;
	[HideInInspector] public string username;
	[HideInInspector] public int buildingNumber;
	//[HideInInspector] public float[][] templateBuilding={{},{}};

	public Button[] troopsButton;

	public string[] listNamaBuilding = {"Canon","Mage","Archer","Wall","Barrack","Gem Reactor","Gold Reactor"};
	public string[,] opponent;
	public int[] capacity = new int[3]{25,50,75};

	public float []  HPTroop= new float[6];
	public float[] AttackTroop=new float[6];
	public float[] AttackWallTroop=new float[6];
	public float[] AttackSpeedTroop=new float[6];
	public float[] AttackRangeTroop=new float[6];
	public float[] MoveSpeedTroop=new float[6];
	public int[] BuyTroop=new int[6];
	public int[] CapacityTroop=new int[6];

	public Color[,] Colors=new Color[7,3]{
		{new Color(),new Color(),new Color()},
		{new Color(),new Color(),new Color()},
		{new Color(),new Color(),new Color()},
		{new Color(),new Color(),new Color()},
		{new Color(),new Color(),new Color()},
		{new Color(),new Color(),new Color()},
		{new Color(),new Color(),new Color()}
	};

	private CameraController camcontrol;
	private Vector3 mouseClickedPos;
	private int prevSelectedTroopsPrefab;
	private List<Dictionary<string, object>> buildingDataFromDB;
	private DBManager DBm;
	public Slider slider;
	private int stolenGold;
	private int stolenGem;
	[HideInInspector] public int reactorAmount=0;
	public GameObject stolenGoldText;
	public GameObject stolenGemText;

	public void updateHP(float dmg){
		currentenemyhp -= dmg;
		slider.value = currentenemyhp/enemyhp;
		if(currentenemyhp<=0){
			ResultPanel();
		}
	}
	public void deleteTower(string _namatower, int _gold){
		DBm.deleteTower (_namatower, _gold);
		plusGold (_gold);
		for (int i=0; i<buildingList.Count; i++){
			if (buildingList [i] != null) {
				if (buildingList[i].GetComponent<TowerController>().namatower.Equals(_namatower)){
					Debug.Log ("gm"+_namatower);
					Destroy (buildingList [i]);
					buildingList [i].GetComponent<TowerController> ().buildingInfoPanel = null;
					HPBarTower [i].gameObject.SetActive (false);
				}
			}
		}
	}
	public void upgradeTower(string _namatower, int _gold){
		DBm.upgradeTower (_namatower, _gold);
		minusGold (_gold);
		for (int i=0; i<buildingList.Count; i++){
			if (buildingList [i] != null) {
				TowerController tc=buildingList [i].GetComponent<TowerController> ();
				if (tc.namatower.Equals(_namatower)){
					tc.towerLevel += 1;
					tc.hp = DBm.HP [tc.buildingCode, tc.towerLevel];
					TowerController.ColorizeBuilding(tc.towerLevelIndicator,TowerController.levelColor[tc.towerLevel]);
				}
			}
		}
	}

	public void repairTower(string _namatower, int _gold){
		DBm.repairTower (_namatower, _gold);
		minusGold (_gold);
		for (int i=0; i<buildingList.Count; i++){
			if (buildingList [i] != null) {
				if (buildingList[i].GetComponent<TowerController>().namatower.Equals(_namatower)){
					buildingList [i].GetComponent<TowerController> ().hp = DBm.HP[buildingList [i].GetComponent<TowerController> ().buildingCode,buildingList [i].GetComponent<TowerController> ().towerLevel];
					buildingDataFromDB [i] ["hp"] = DBm.HP [buildingList [i].GetComponent<TowerController> ().buildingCode, buildingList [i].GetComponent<TowerController> ().towerLevel];
					HPBarTower[i].value = float.Parse (buildingDataFromDB [i] ["hp"].ToString ()) / DBm.HP [int.Parse(buildingDataFromDB [i] ["type"].ToString()),int.Parse(buildingDataFromDB [i] ["level"].ToString())];
				}
			
			}
		}
	}

	public void ResultPanel(){
		Time.timeScale=0;//cek apakah ada pengaruh
		DBm.troops = troops;
		if (reactorAmount==0 || (currentenemyhp * 100 / enemyhp) <= 30) {
			SetupStolenResourceInfo(0.3f);
			resultpanel2.gameObject.SetActive (true);
		} else {
			SetupStolenResourceInfo(0.1f);
			resultpanel.gameObject.SetActive (true);
		}
		stolenGoldText.SetActive(true);
		stolenGemText.SetActive(true);
		stolenGoldText.GetComponent<Text>().text=stolenGold.ToString();
		stolenGemText.GetComponent<Text>().text=stolenGem.ToString();
		//DBm.updateTroops (troops);
		//DBm.SendInvasionResult(stolenGold,stolenGem,troops);
		enemyBuildingList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Building"));
		string[,] enemyTower = new string[enemyBuildingList.Count,7];
		for (int i = 0; i<enemyBuildingList.Count; i++) {
			TowerController tc;
			tc=enemyBuildingList [i].GetComponent<TowerController>();
			enemyTower [i, 0] = tc.namatower;
			enemyTower [i, 1] = tc.arah.ToString();
			enemyTower [i, 2] = tc.hp.ToString();
			enemyTower [i, 3] = tc.towerLevel.ToString();
			enemyTower [i, 4] = tc.buildingCode.ToString();
			enemyTower [i, 5] = tc.transform.position.x.ToString();
			enemyTower [i, 6] = tc.transform.position.z.ToString();
		}
		DBm.SendInvasionResult(stolenGold,stolenGem,troops,enemyTower);
		Debug.Log ("ResultPanel");
	}

	public void SetupStolenResourceInfo(float stolenMultiplier){
		currentenemyhp=Mathf.Max(currentenemyhp, 0f);
		float attackedhp=enemyhp-currentenemyhp;
		stolenGem=(int)(stolenMultiplier*(attackedhp/enemyhp)*enemyGem);
		stolenGold=(int)(stolenMultiplier*(attackedhp/enemyhp)*enemyGold);
		//Set text in resultpanel
	}

	public void SetOpponent(){
		GameObject.Find ("Canvas").GetComponent<Opponent> ().showOpponent ();
	}
	public void minusGold(int _gold){
		gold -= _gold;
	}

	public void plusGold(int _gold){
		gold += _gold;
	}

	public void plusTroops(int [] _jumlah, int _cost){
		for (int i = 0; i < 6; i++) {
			troops[i]+=_jumlah[i];
			basetexttroops [i].text = troops [i].ToString ();
		}
		DBm.minusGold (_cost);
		DBm.updateTroops (troops);
	}

	public void AttackPanelToggle(bool isPanelActive){
		underAttackScene.SetActive(isPanelActive);
	}
	void Start () {
		Time.timeScale=1;
		DBm = GameObject.Find ("DBManager").GetComponent<DBManager> ();

		if (gameplayMode == 0) {
			Debug.Log ("masuk 0");
			//GameObject.Find ("DBManager").GetComponent<DBManager> ().GetBuildingData ();
			opponent = DBm.opponent;
			buildingDataFromDB = DBm.allResultList;
			int trooptype=0;
			foreach(Dictionary<string,object> items in DBm.allmetadata){
				HPTroop[trooptype]=float.Parse(items["hp"].ToString());
				BuyTroop[trooptype]=int.Parse(items["buy"].ToString());
				AttackTroop[trooptype]=float.Parse(items["attack"].ToString());
				AttackWallTroop[trooptype]=float.Parse(items["attackwall"].ToString());
				AttackSpeedTroop[trooptype]=float.Parse(items["attackspeed"].ToString());
				MoveSpeedTroop[trooptype]=float.Parse(items["movespeed"].ToString());
				AttackRangeTroop[trooptype]=float.Parse(items["attackrange"].ToString());
				CapacityTroop[trooptype++]=int.Parse(items["capacity"].ToString());
			}

			DBm.HPTroop = HPTroop;
			DBm.AttackTroop = AttackTroop;
			DBm.AttackRangeTroop = AttackRangeTroop;
			DBm.MoveSpeedTroop = MoveSpeedTroop;
			DBm.AttackWallTroop = AttackWallTroop;
			//Debug.Log(DBm.allmetadatatower.Count+"dsdsd");


			gold = DBm.gold;
			basegold.text = gold.ToString ();
			gem = DBm.gem;
			basegem.text = gem.ToString ();
			username = DBm.username;
			baseteam.text = username;
			buildingNumber = DBm.buildingNumber;
			troops = DBm.troops;
			
			//listening to gold and gem change
			DBm.listenToEventPriceChange();
			DBm.listenToGoldChange();
			DBm.listenToGemChange();
			DBm.listenToIsAttackedChange();
			
			for (int i = 0; i < 6; i++) {
				basetexttroops [i].text = troops [i].ToString ();
				pricetexttroops [i].text = BuyTroop [i].ToString ();
				capacitytexttroops [i].text = "Capacity : "+CapacityTroop [i].ToString ();
			}
			for (int i = 0; i < buildingDataFromDB.Count; i++) {

				float x = float.Parse (buildingDataFromDB [i] ["x"].ToString ());
				float y = float.Parse (buildingDataFromDB [i] ["y"].ToString ());
				int type = int.Parse (buildingDataFromDB [i] ["type"].ToString ());
				int level = int.Parse (buildingDataFromDB [i] ["level"].ToString ());
				int arah =int.Parse (buildingDataFromDB [i] ["arah"].ToString ());

				Vector3 spawnPosBuilding = new Vector3 (x, 0, y);
				int[] sells = new int[3];
				for (int j = 0; j < 3; j++) {
					sells [j] = DBm.sell [type, j];
				}
				int[] upgrades = new int[3];
				for (int j = 0; j < 3; j++) {
					upgrades [j] = DBm.upgrade [type, j];
				}
				int[] repairs = new int[3];
				for (int j = 0; j < 3; j++) {
					repairs [j] = DBm.repair [type, j];
				}

				GameObject tempBuilding = (GameObject)Instantiate (buildingPrefabsList [type], spawnPosBuilding, Quaternion.identity);
				tempBuilding.GetComponent<TowerController> ().SetupTower (
					true,
					type,
					level,
					DBm.Attack [type, level],
					DBm.HP [type, level],
					float.Parse (buildingDataFromDB [i] ["hp"].ToString ()),
					DBm.AttackSpeed [type, level],
					arah,
					DBm.namatower[i]
				);
				HPBarTower.Add((Slider)Instantiate (HPBar));
				HPBarTower [i].value = float.Parse (buildingDataFromDB [i] ["hp"].ToString ()) / DBm.HP [type, level];
				HPBarTower [i].transform.SetParent(GameObject.Find("DetailTim").transform,false);
				HPBarTower [i].transform.localScale = new Vector3 (0.7f, 1f, 1f);
				HPBarTower [i].transform.position = Camera.main.WorldToScreenPoint((Vector3.up * 20)+tempBuilding.transform.position);
			}
			isPlacementDone = true;//entah kenapa jika di init di atas bisa kembali jadi false;
			projectedPlane = new Plane (Vector3.up, groundPlane.transform.position);
			camcontrol=GameObject.Find("MainCamera").GetComponent<CameraController>();

			// Load data building yang sudah ada

			buildingList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Building"));
			originalBuildingList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Building"));
			SetOpponent();
		} else if (gameplayMode == 1) {
			Debug.Log ("masuk 1");
			buildingDataFromDB = DBm.allResultList;
			/*int trooptype=0;
			foreach(Dictionary<string,object> items in DBm.allmetadata){
				HPTroop[trooptype]=float.Parse(items["hp"].ToString());
				BuyTroop[trooptype]=int.Parse(items["buy"].ToString());
				AttackTroop[trooptype]=float.Parse(items["attack"].ToString());
				AttackSpeedTroop[trooptype]=float.Parse(items["attackspeed"].ToString());
				MoveSpeedTroop[trooptype]=float.Parse(items["movespeed"].ToString());
				AttackRangeTroop[trooptype]=float.Parse(items["attackrange"].ToString());
				CapacityTroop[trooptype++]=int.Parse(items["capacity"].ToString());
				Debug.Log ("HP" + HPTroop[0]);
			}*/
			myTeamText.text = DBm.username;
			enemyTeamText.text = DBm.enemyteam;
			enemyGold=DBm.enemygold;
			enemyGem=DBm.enemygem;
			myGold=DBm.gold;
			myGem=DBm.gem;
			myGoldText.text = myGold.ToString ();
			myGemText.text = myGem.ToString ();
			enemyGoldText.text = enemyGold.ToString ();
			enemyGemText.text = enemyGem.ToString ();
			troops = DBm.troops;
			for (int i = 0; i < 6; i++) {
				texttroops [i].text = troops [i].ToString ();
			}
			for (int i = 0; i < buildingDataFromDB.Count; i++) {

				float x = float.Parse (buildingDataFromDB [i] ["x"].ToString ());
				float y = float.Parse (buildingDataFromDB [i] ["y"].ToString ());
				int type = int.Parse (buildingDataFromDB [i] ["type"].ToString ());
				int level = int.Parse (buildingDataFromDB [i] ["level"].ToString ());
				int arah =int.Parse (buildingDataFromDB [i] ["arah"].ToString ());
				Vector3 spawnPosBuilding = new Vector3 (x, 0, y);
				int[] sells = new int[3];
				for (int j = 0; j < 3; j++) {
					sells [j] = DBm.sell [type, j];
				}
				int[] upgrades = new int[3];
				for (int j = 0; j < 3; j++) {
					upgrades [j] = DBm.upgrade [type, j];
				}
				int[] repairs = new int[3];
				for (int j = 0; j < 3; j++) {
					repairs [j] = DBm.repair [type, j];
				}
				enemyhp += float.Parse (buildingDataFromDB [i] ["hp"].ToString ());
				GameObject tempBuilding = (GameObject)Instantiate (buildingPrefabsList [type], spawnPosBuilding, Quaternion.identity);
				tempBuilding.GetComponent<TowerController> ().SetupTower (
					true,
					type,
					level,
					DBm.Attack [type, level],
					DBm.HP [type, level],
					float.Parse (buildingDataFromDB [i] ["hp"].ToString ()),
					DBm.AttackSpeed [type, level],
					arah,
					DBm.namatower[i]
				);

				/* Jika GoldReactor atau GemReactor */
				if(type==5 || type==6) reactorAmount++;

				HPBarTower.Add((Slider)Instantiate (HPBar));
				HPBarTower [i].value = float.Parse (buildingDataFromDB [i] ["hp"].ToString ()) / DBm.HP [type, level];
				HPBarTower [i].transform.SetParent(GameObject.Find("DetailTim").transform,false);
				HPBarTower [i].transform.localScale = new Vector3 (0.7f, 1f, 1f);
				HPBarTower [i].transform.position = Camera.main.WorldToScreenPoint((Vector3.up * 20)+tempBuilding.transform.position);
			}
			currentenemyhp = enemyhp;
			Debug.Log(currentenemyhp);
			isPlacementDone = true;//entah kenapa jika di init di atas bisa kembali jadi false;
			projectedPlane = new Plane (Vector3.up, groundPlane.transform.position);
			camcontrol = GameObject.Find ("MainCamera").GetComponent<CameraController> ();
			// Load data building yang sudah ada

			buildingList = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Building"));
			originalBuildingList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Building"));
		}
	}

	public void startAttack(string attacked){
		DBm.unlistenToIsAttackedChange();
		DBm.minusGold(1000);//TODO: Hardcode price
		DBm.GetGoldGem (attacked);
		DBm.setIsAttacked (attacked);
		DBm.GetBuildingData (attacked);
	}

	public void ConfirmAttackResult(){
		DBm.LoadGameToBaseScene (false);
	}

	public delegate void Aksi(int lastAttackInSecond);
	public delegate void AksiVoid();

	public void getLastAttackedSeconds(string teamname,Aksi callback){
			DBm.getLastAttacked(teamname,callback);
			//return DBm.lastAttackedSecs;
	}
	
	// public int getLastAttackedSeconds(string teamname){
	// 	DBm.getLastAttacked(teamname);
	// 	return DBm.lastAttackedSecs;
	// }

	public void Retreat(){
		//GameObject.Find ("Canvas").GetComponent<LoadingBar> ().LoadLevel(1);
		DBm.troops = troops;
		enemyBuildingList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Building"));
		string[,] enemyTower = new string[enemyBuildingList.Count,7];
		for (int i = 0; i<enemyBuildingList.Count; i++) {
			TowerController tc;
			tc=enemyBuildingList [i].GetComponent<TowerController>();
			enemyTower [i, 0] = tc.namatower;
			enemyTower [i, 1] = tc.arah.ToString();
			enemyTower [i, 2] = tc.hp.ToString();
			enemyTower [i, 3] = tc.towerLevel.ToString();
			enemyTower [i, 4] = tc.buildingCode.ToString();
			enemyTower [i, 5] = tc.transform.position.x.ToString();
			enemyTower [i, 6] = tc.transform.position.z.ToString();
		}
		DBm.SendInvasionResult(0,0,troops,enemyTower);
		Debug.Log ("retreat"+enemyTower.GetLength(0));
		DBm.LoadGameToBaseScene (false);
		//GameObject.Find ("Canvas").GetComponent<LoadingBar> ().LoadLevel(1);
	}

	void OnApplicationPause(bool isPaused){
        // if(isPaused && gameplayMode==1){
        // 	Retreat();
        // }
		//FIXME:
		//FIXME: kalau sudah selesai debugging, jangan lupa dinyalakan
		//FIXME:
    }

	void Update () {
		gold = DBm.gold;
		if (gameplayMode == 0) {
			basegold.text = DBm.gold.ToString ();
			basegem.text = DBm.gem.ToString ();
			baseteam.text = username;
		} else if (gameplayMode == 1) {
			for (int i = 0; i < 6; i++) {
				texttroops [i].text = troops [i].ToString ();
			}
			targetTime -= Time.deltaTime;
			time.text = Mathf.Floor(targetTime).ToString ()+" s";
			if (targetTime <= 0.0f) {
				Time.timeScale=0f;
				ResultPanel();
			}
		}
		for (int i = 0; i < buildingDataFromDB.Count; i++) {
		   float x = float.Parse (buildingDataFromDB [i] ["x"].ToString ());
		   float y = float.Parse (buildingDataFromDB [i] ["y"].ToString ());
		   Vector3 spawnPosBuilding = new Vector3 (x, 0, y);
		   HPBarTower [i].transform.position = Camera.main.WorldToScreenPoint((Vector3.up * 20)+spawnPosBuilding);
		}
	}

	public GameObject DetermineTroopsTarget(Vector3 troopsPos, bool includeWall=false){
		GameObject targetBuilding=null;
		if (buildingList.Count > 0) {
			float minDistance = Mathf.Infinity;
			float curDistance = 0f;
			targetBuilding = buildingList [0];
			foreach (GameObject g in buildingList) {
				//if wall, then ignore
				if (g != null && ((!includeWall && g.layer!=8) || includeWall)) {
					curDistance = CalculateDistance (troopsPos, g.transform.position);
					if (curDistance < minDistance) {
						targetBuilding = g;
						minDistance = curDistance;
					}
				}
			}
		}
		return targetBuilding;
	}

	public float CalculateDistance(Vector3 troopsPos, Vector3 buildingPos){
		return Mathf.Sqrt (Mathf.Pow (buildingPos.x - troopsPos.x, 2) + Mathf.Pow (buildingPos.z - troopsPos.z, 2));
	}

	public void ConfirmPlacement(bool isPlacementAccepted){
		currentBuildingPlacement.ConfirmPlacement (isPlacementAccepted);
	}

	public void InstantiateBuilding(int buildingCode){
		if (isPlacementDone && DBm.gold >= DBm.towerCost [buildingCode]) {
			SetBuildingColliderStatus (false);
			GameObject tempBuilding = (GameObject)Instantiate (buildingPrefabsList [buildingCode], transform.position, Quaternion.identity);

			currentBuildingPlacement = tempBuilding.GetComponent<TowerController> ();
			camcontrol.currentBuildingPlacement = currentBuildingPlacement;
			isPlacementDone = false;
		} else {
			Debug.Log (DBm.gold);
			Debug.Log (DBm.towerCost [buildingCode]);
			Debug.Log ("Uang kurang");		
		}
	}
	public void FinishPlacement(bool saveToDB, int arah, int type, int x, int y){
		isPlacementDone = true;
		camcontrol.currentBuildingPlacement = null;
		SetBuildingColliderStatus (true);
		if (saveToDB) {
			DBm.addTower (arah, type, x,y,++buildingNumber, DBm.towerCost[type]);
			buildingDataFromDB.Add (new Dictionary<string,object>(){{"x",x},{"y",y},{"hp",DBm.HP [type, 0]}});

			HPBarTower.Add((Slider)Instantiate (HPBar));
			HPBarTower [buildingDataFromDB.Count-1].value = float.Parse (buildingDataFromDB [buildingDataFromDB.Count-1] ["hp"].ToString ()) / DBm.HP [type, 0];
			HPBarTower [buildingDataFromDB.Count-1].transform.SetParent(GameObject.Find("DetailTim").transform,false);
			HPBarTower [buildingDataFromDB.Count-1].transform.localScale = new Vector3 (0.7f, 1f, 1f);
			HPBarTower [buildingDataFromDB.Count-1].transform.position = Camera.main.WorldToScreenPoint((Vector3.up * 20)+new Vector3(x,y,0));
		}
	}

	public void RotateBuilding(){
		currentBuildingPlacement.RotateBuilding ();
	}

	public void AddBuildingToList(GameObject newBuilding){
		buildingList.Add (newBuilding);
	}

	public void SelectTroopsPrefab(int troopsIndex){ Debug.Log (troopsIndex);
		Debug.Log ("current " + currentSelectedTroopsPrefab);
		ColorBlock colors = troopsButton [troopsIndex].colors;
		if (currentSelectedTroopsPrefab != -1) {
			colors.normalColor = Color.white;
			colors.highlightedColor = Color.white;
			troopsButton [currentSelectedTroopsPrefab].colors = colors;
		}
		if (currentSelectedTroopsPrefab != troopsIndex) {
			currentSelectedTroopsPrefab = troopsIndex;
			colors.normalColor = Color.yellow;
			colors.highlightedColor = Color.yellow;
			troopsButton[troopsIndex].colors = colors;
		} else {
			currentSelectedTroopsPrefab = -1;
		}
	}

	public void SpawnTroops(Vector3 clickedPos){
		if (gameplayMode == 1) {
			if (currentSelectedTroopsPrefab!=-1) {
				prevSelectedTroopsPrefab = currentSelectedTroopsPrefab;
				mouseClickedPos = clickedPos;
				StartCoroutine ("ClickButtonOrSpawnTroopsChecker");
			}
		}
	}

	public void CheckAttackerWin(){
		//ATTACKER WIN JIKA SEMUA BUILDING HANCUR ATAU CUKUP CASTLE SAJA???
		//JIKA CUKUP CASTLE SAJA, MAKA BERI CODE PADACASTLE AGAR HANYA JIKA CASTLE HANCUR SAJA PENGECEKAN AKAN BERJALAN
	}

	public void CheckDefenderWin(){
		//TAMBAHKAN COUNTER KETIKA ADA TROOPS DISPAWN, DAN KURANGI KETIKA MATI. PANGGIL FUNCTION CHECK TIAP KALI TROOPS MATI
		//CEK APAKAH JUMALH 0 DAN JUMLAH PERSEDIAAN TROOPS JUGA 0
	}

	IEnumerator ClickButtonOrSpawnTroopsChecker(){
		yield return new WaitForSeconds(0.05f);
		if (prevSelectedTroopsPrefab == currentSelectedTroopsPrefab) {
			Ray touchRay = Camera.main.ScreenPointToRay (mouseClickedPos);
			float rayDistance;
			if (projectedPlane.Raycast (touchRay, out rayDistance)) {
				//ganti cara menambahkan y yang lebih elegant. menghindari placement boundaries
				Vector3 tempSpawnPos = currentSelectedTroopsPrefab > 3 ? new Vector3 (touchRay.GetPoint (rayDistance).x, 10f, touchRay.GetPoint (rayDistance).z) : new Vector3 (touchRay.GetPoint (rayDistance).x, touchRay.GetPoint(rayDistance).y+0.5f, touchRay.GetPoint (rayDistance).z);
				if (SpawnPointValidation (tempSpawnPos)) {
					if (troops [currentSelectedTroopsPrefab] > 0) {
						troops [currentSelectedTroopsPrefab] -= 1;
						GameObject troopsTemp = (GameObject)Instantiate (troopsPrefabsList [currentSelectedTroopsPrefab], tempSpawnPos, Quaternion.identity);
						Debug.Log ("ini yg dipilih"+currentSelectedTroopsPrefab);
						troopsTemp.GetComponent<TroopsController> ().setUpTroops (DBm.AttackTroop [currentSelectedTroopsPrefab], DBm.AttackWallTroop [currentSelectedTroopsPrefab], DBm.HPTroop [currentSelectedTroopsPrefab], DBm.MoveSpeedTroop [currentSelectedTroopsPrefab], DBm.AttackRangeTroop [currentSelectedTroopsPrefab]);
//						newHPBarTroop = (Slider)Instantiate (HPBar);
//						newHPBarTroop.value = 0.5;
//						newHPBarTroop.transform.SetParent(GameObject.Find("DetailTim").transform,false);
//						newHPBarTroop.transform.position = Camera.main.WorldToScreenPoint((Vector3.up * 20)+troopsTemp.transform.position);
					}
				}
			}
		}
		//StopCoroutine("ClickButtonOrSpawnTroopsChecker");
	}

	public bool SpawnPointValidation(Vector3 tempSpawnPos){
		return !(tempSpawnPos.x < defenderAreaLimit [1] && tempSpawnPos.x > defenderAreaLimit [0] && tempSpawnPos.z < defenderAreaLimit [3] && tempSpawnPos.z > defenderAreaLimit [2]);
	}

	public void SetBuildingColliderStatus(bool status){
		foreach (GameObject b in buildingList) {
			if (b != null) {
				b.GetComponent<CapsuleCollider> ().enabled = status; //expensive process, cari cara yg lebih ringan
			}
		}
	}

	public void LogoutAccount(){
		if (DBm) {
			DBm.logoutFromFirebase ();
		} else {
			Debug.Log ("tidak ada data");
		}
	}
}
