﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class LoadingBar : MonoBehaviour {
	public GameObject panel;
	public Slider slider;
	public Text progressText;
	DBManager DBm;

	public void LoginAccount(){
		DBm = GameObject.Find ("DBManager").GetComponent<DBManager> ();
		DBm.SendLogin();
	}

	public void LoadLevel (int sceneIndex){
		//StartCoroutine (LoadAsynchronously(sceneIndex));
		StartCoroutine ("LoadAsynchronously",sceneIndex);
	}

	IEnumerator LoadAsynchronously (int sceneIndex){
		//AsyncOperation operation = SceneManager.LoadSceneAsync (sceneIndex);
		float count = 0;
		while (count<1000) {
			count++;
			panel.SetActive (true);
			float progress = Mathf.Clamp01 (count/ 1000);
			slider.value = progress;
			//progressText.text = "" + (int) (progress * 100f) + '%';
			yield return null;
		}
		//Debug.Log("LoadAsynchronously");
		//StopCoroutine("LoadAsynchronously");
	}
}
