﻿
using UnityEngine;
using System.Collections.Generic;

public class CameraController : MonoBehaviour {

	private Vector3 curPos;
	public float panSpeed;
	public Vector2 xPanLimit;
	public Vector2 zPanLimit;
	public float zoomSpeed;
	public Vector2 zoomLimit;
	[HideInInspector] public TowerController currentBuildingPlacement;
	private Camera cam;
	private float changeSize;
	private float extraChangeSize;
	private GameManager gm;

	private List<Lean.Touch.LeanFinger> fingers;
	private float pinchRatio;
	private Vector3 worldDelta;

	private bool IgnoreGuiFingers=true;
	private int RequiredFingerCount=0;
	private float WheelSensitivity=0f;
	private float Distance=13f;
	private float Sensitivity=1f;

	void Start(){
		gm=GameObject.Find("GameManager").GetComponent<GameManager>();
		cam=GetComponent<Camera>();
		curPos=transform.position;
	}

	void LateUpdate () {
		if (!currentBuildingPlacement || (currentBuildingPlacement && !gm.isBuildingPressed)) {
			fingers = Lean.Touch.LeanTouch.GetFingers (IgnoreGuiFingers, RequiredFingerCount);

			worldDelta = Lean.Touch.LeanGesture.GetWorldDelta (fingers, Distance);
			if (worldDelta.x != 0) {
				curPos.x -= worldDelta.x * panSpeed * Time.deltaTime;
			}
			if (worldDelta.z != 0) {
				curPos.z -= worldDelta.z * panSpeed * Time.deltaTime;
			}

			pinchRatio = Lean.Touch.LeanGesture.GetPinchRatio (fingers, WheelSensitivity) * 2 - 2;
			if (pinchRatio != 0) {
				changeSize = pinchRatio * zoomSpeed * Time.deltaTime;
				cam.orthographicSize += changeSize;
				cam.orthographicSize = Mathf.Clamp (cam.orthographicSize, zoomLimit.x, zoomLimit.y);
				if (cam.orthographicSize > zoomLimit.x && cam.orthographicSize < zoomLimit.y) {
					extraChangeSize = 0.25f*changeSize;
					xPanLimit.x += extraChangeSize;
					xPanLimit.y -= extraChangeSize;
					zPanLimit.x += extraChangeSize;
					zPanLimit.y -= extraChangeSize;
					//mungkin bisa bug, krn idealnya adalah cek apakah sebelum dan setelah +changesize ada perubahan atau tidak
				}
			}
			curPos.x = Mathf.Clamp (curPos.x, xPanLimit.x, xPanLimit.y);
			curPos.z = Mathf.Clamp (curPos.z, zPanLimit.x, zPanLimit.y);
			transform.position = curPos;
			if (currentBuildingPlacement) {
				//ganti juga xcamoffset dan ycamoffset????
				if (currentBuildingPlacement.guiPlacement) {//if dobel, mungkin agak berat. Pikirkan cara yang lebih elegan
					currentBuildingPlacement.RepositionPlacementButtons ();
				}
			}
		}
	}
}
