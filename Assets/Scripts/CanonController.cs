﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonController : MonoBehaviour {

	private bool isDestroyed = false;
	public float canonDmg;

	void Update () {
		if (isDestroyed) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Troops") {
			other.gameObject.GetComponent<TroopsController> ().Hit (canonDmg);
			isDestroyed = true;
		}
	}
}
