using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System;

public class DBManager: MonoBehaviour {
	public static DBManager Instance;
	public DatabaseReference db;
	public InputField name;
	public string username;
	public InputField password;
	private string passwd;
	Dictionary<string, object> results;
	Dictionary<string, object> detailresults;
	public List<Dictionary<string, object>> allResultList;
	public List<Dictionary<string, object>> allmetadata;
	public List<List<Dictionary<string, object>>> allmetadatatower;
	public int gold;
	public int gem;
	public string enemyteam;
	public int enemygold;
	public int enemygem;
	public int buildingNumber;
	public int[] troops = new int[6];
	public string isAttacked;
	public long lastAttacked;
	public string isEnemyAttacked;
	public long lastEnemyAttacked;
	public List<string> namatower;
	public string[,] opponent = new string[19,3];
	public Firebase.Auth.FirebaseAuth auth;

	private GameManager gm;
	private string prevCycleIsAttacked="";
	public int event_price;
	public int lastAttackedSecs;

	//for usage in loginPanel
	public GameObject WrongUserPwdPanel;
	public Text WrongUserPwdPanelMessage;

	private int reactorAmount;
	private string custom_token;

	public float []  HPTroop= new float[6];
	public float[] AttackTroop=new float[6];
	public float[] AttackWallTroop=new float[6];
	public float[] AttackRangeTroop=new float[6];
	public float[] MoveSpeedTroop=new float[6];

	public float [,]  HP= new float[7,3]{{100,200,400},{125,175,275},{150,250,400},{300,600,600},{150,300,450},{1000,1200,1500},{800,1000,1200}};
	public int [,] sell = new int[7,3]{{500,1500,3500},{1000,1900,3250},{800,1500,2500},{100,200,0},{0,0,0},{0,0,0},{0,0,0}};
	public int [,] repair = new int[7,3]{{250,500,1500},{500,1000,2000},{250,500,1000},{50,100,0},{1000,1000,1000},{250,750,1200},{250,500,1000}};
	public int[,] upgrade= new int[7,3]{{3500,7500,0},{4800,7500,0},{4500,7000,0},{500,0,0},{3000,6000,0},{2000,3500,0},{1500,3000,0}};
	public float[,] Attack=new float[7,3]{{75,150,250},{125,175,275},{60,120,200},{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
	public float[,] AttackSpeed=new float[7,3]{{2.5f,2f,1.5f},{3f,2.5f,2f},{2f,1.5f,1f},{0f,0f,0f},{0f,0f,0f},{0f,0f,0f},{0f,0f,0f}};
	public float[,] Range=new float[7,3]{{2.5f,2f,1.5f},{3f,2.5f,2f},{2f,1.5f,1f},{0f,0f,0f},{0f,0f,0f},{0f,0f,0f},{0f,0f,0f}};
	public int[] towerCost = { 1500, 3000, 2500, 300 , 0, 0};

	void Start(){
		lastAttackedSecs = 0;
	}

	void Awake() {
		if((GameObject.FindGameObjectsWithTag("DBManager")).Length>1){
			Destroy (gameObject);
		}else{
			DontDestroyOnLoad (this);
			if (Instance == null) Instance = this;
			// Set this before calling into the realtime database.
			FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://irgl2018.firebaseio.com/");
			auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
			// Get the root reference location of the database, from which we will operate on the database.
			db = FirebaseDatabase.DefaultInstance.RootReference;

			if(PlayerPrefs.GetString("token","")!=""){
				username=PlayerPrefs.GetString("username", "");
				StartCoroutine("RefreshCustomToken",PlayerPrefs.GetString("token",""));
			}
		}	
	}

	public void SendLogin(){
		StartCoroutine("Login");
	}
	public IEnumerator Login(){
		WWWForm form = new WWWForm();
		name=GameObject.Find("tfUser").GetComponent<InputField>();
		password=GameObject.Find("tfPass").GetComponent<InputField>();
		username = name.text;
		form.AddField("tim", name.text);
		form.AddField("pwd", password.text);

		UnityWebRequest www = UnityWebRequest.Post("https://us-central1-irgl2018.cloudfunctions.net/irgl_auth", form);
		yield return www.Send ();

		if(www.isNetworkError || www.isHttpError) {
			Debug.Log(www.error);
			Debug.Log(www.url);
			OpenPanel();
		}
		else {
			custom_token = www.downloadHandler.text;
			Debug.Log (custom_token);
			auth.SignInWithCustomTokenAsync (custom_token).ContinueWith (task => {
				if (task.IsCanceled) {
					Debug.LogFormat ("SignInWithCustomTokenAsync was canceled");
					return;
				}
				if (task.IsFaulted) {
					Debug.LogFormat ("SignInWithCustomTokenAsync encountered an error");
					OpenPanel();
					return;
				}
				Firebase.Auth.FirebaseUser newUser = task.Result;
				Debug.LogFormat ("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
				
				PlayerPrefs.SetString("username", name.text);
				PlayerPrefs.SetString("password", password.text);
				PlayerPrefs.SetString("token", custom_token);

				LoadGameToBaseScene();
			});
		}
		//StopCoroutine("Login");
	}

	public IEnumerator RefreshCustomToken(string custom_token){
		WWWForm form = new WWWForm();
		form.AddField("customToken", custom_token);

		UnityWebRequest www = UnityWebRequest.Post("https://us-central1-irgl2018.cloudfunctions.net/irgl_auth_relogin", form);
		yield return www.Send ();

		if(www.isNetworkError || www.isHttpError) {
			Debug.Log(www.error);
			Debug.Log(www.url);
			OpenPanel();
			PlayerPrefs.DeleteKey("username");
			PlayerPrefs.DeleteKey("password");
			PlayerPrefs.DeleteKey("token");
		}
		else {
			custom_token = www.downloadHandler.text;
			AutoLogin(custom_token);
		}
		//StopCoroutine("Login");
	}

	public void AutoLogin(string custom_token){
		auth.SignInWithCustomTokenAsync (custom_token).ContinueWith (task => {
			if (task.IsCanceled) {
				Debug.LogFormat ("SignInWithCustomTokenAsync was canceled");
				return;
			}
			if (task.IsFaulted) {
				Debug.LogFormat ("SignInWithCustomTokenAsync encountered an error");
				OpenPanel();
				PlayerPrefs.DeleteKey("username");
				PlayerPrefs.DeleteKey("password");
				PlayerPrefs.DeleteKey("token");
				return;
			}
			
			Firebase.Auth.FirebaseUser newUser = task.Result;
			PlayerPrefs.SetString("token", custom_token);

			Debug.LogFormat ("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);

			LoadGameToBaseScene();
		});
	}
	public void LoadGameToBaseScene(bool checkTroops=true){
		GetGoldGem();
		GetBuildingData();
		GetMetadataBuilding();
		GetMetadataTroop();
		if (checkTroops) {
			GetTroops();
		}
		GetOpponent();
		GetAttacked();
		//listenToGoldChange();
		//listenToGemChange();
		GameObject.Find ("Canvas").GetComponent<LoadingBar> ().LoadLevel(1);
	}
	public void OpenPanel(/*string message*/){
		//WrongUserPwdPanelMessage.text=message;
		WrongUserPwdPanel.SetActive(true);
	}
	public void ClosePanel(/*string message*/){
		//WrongUserPwdPanelMessage.text=message;
		WrongUserPwdPanel.SetActive(false);
	}

	public delegate void Action();

	void EventPriceChanged(object sender, ValueChangedEventArgs args){
		if (args.DatabaseError != null) {
			Debug.LogError(args.DatabaseError.Message);
			return;
		}
		event_price=int.Parse(args.Snapshot.Value.ToString());
		Debug.Log("Change event_price");
	}

	public void listenToEventPriceChange(){
		FirebaseDatabase.
		DefaultInstance.
		GetReference("event_price").
		ValueChanged += EventPriceChanged;
	}

	public void unlistenToEventPriceChange(){
		FirebaseDatabase.
		DefaultInstance.
		GetReference("event_price").
		ValueChanged -= EventPriceChanged;
	}

	void GoldChanged(object sender, ValueChangedEventArgs args){
		if (args.DatabaseError != null) {
			Debug.LogError(args.DatabaseError.Message);
			return;
		}
		gold=int.Parse(args.Snapshot.Value.ToString());
		Debug.Log("Change Gold");
	}

	public void listenToGoldChange(){
		FirebaseDatabase.
			DefaultInstance.
			GetReference("tim").
			Child(username).
			Child("gold").
			ValueChanged += GoldChanged;
	}

	public void unlistenToGoldChange(){
		FirebaseDatabase.
			DefaultInstance.
			GetReference("tim").
			Child(username).
			Child("gold").
			ValueChanged -= GoldChanged;
	}

	void GemChanged(object sender, ValueChangedEventArgs args){
		if (args.DatabaseError != null) {
			Debug.LogError(args.DatabaseError.Message);
			return;
		}
		gem=int.Parse(args.Snapshot.Value.ToString());
		Debug.Log("Change gem");
	}

	public void listenToGemChange(){
		FirebaseDatabase.
		DefaultInstance.
		GetReference("tim").
		Child(username).
		Child("gem").
		ValueChanged += GemChanged;
	}

	public void unlistenToGemChange(){
		FirebaseDatabase.
		DefaultInstance.
		GetReference("tim").
		Child(username).
		Child("gem").
		ValueChanged -= GemChanged;
	}

	void IsAttackedChanged(object sender, ValueChangedEventArgs args){
		if (args.DatabaseError != null) {
			Debug.LogError(args.DatabaseError.Message);
			return;
		}
		
		//whether to show panel, 
		//if the previous is ""
		//means now it's attacked'
		
		isAttacked=args.Snapshot.Value.ToString();

		bool x;
		if(prevCycleIsAttacked.Equals("") && !isAttacked.Equals(""))x=true;
		else x=false;

		if(prevCycleIsAttacked!=isAttacked){
			Debug.Log("isinya isattacked"+isAttacked);
			if(isAttacked.Equals("")){
				GetBuildingData();
				Debug.Log ("masuk om");
				GameObject.Find("GameManager").GetComponent<GameManager>().AttackPanelToggle(false);
			}else{
				GameObject.Find("GameManager").GetComponent<GameManager>().AttackPanelToggle(true);
			}
			prevCycleIsAttacked=isAttacked;
		}else{
			prevCycleIsAttacked=isAttacked;
		}
		Debug.Log("Change isAttacked");
	}

	public void listenToIsAttackedChange(){
		FirebaseDatabase.
			DefaultInstance.
			GetReference("tim").
			Child(username).
			Child("isAttacked").
			ValueChanged += IsAttackedChanged;
	}

	public void unlistenToIsAttackedChange(){
		FirebaseDatabase.
			DefaultInstance.
			GetReference("tim").
			Child(username).
			Child("isAttacked").
			ValueChanged -= IsAttackedChanged;
	}

	// public void listenTolastAttackedChange(Action notifPanelOpen){
	// 	FirebaseDatabase.
	// 	DefaultInstance.
	// 	GetReference("tim").
	// 	Child(username).
	// 	Child("lastAttacked").
	// 	ValueChanged += ( (object sender, ValueChangedEventArgs args)=> {
	// 		if (args.DatabaseError != null) {
	// 			Debug.LogError(args.DatabaseError.Message);
	// 			return;
	// 		}
	// 		lastAttacked=long.Parse(args.Snapshot.Value.ToString());
	// 		Debug.Log("Change lastAttacked");
	// 		Debug.Log(lastAttacked);
	// 		notifPanelOpen();
	// 	});
	// }

	// public void unlistenTolastAttackedChange(){
	// 	FirebaseDatabase.
	// 	DefaultInstance.
	// 	GetReference("tim").
	// 	Child(username).
	// 	Child("lastAttacked").
	// 	ValueChanged -= ( (object sender, ValueChangedEventArgs args)=> {
	// 		if (args.DatabaseError != null) {
	// 			Debug.LogError(args.DatabaseError.Message);
	// 			return;
	// 		}
	// 		lastAttacked=long.Parse(args.Snapshot.Value.ToString());
	// 		Debug.Log("Change lastAttacked");
	// 		Debug.Log(lastAttacked);
	// 	});
	// }

	public void minusGold(int _gold,bool isBuyingTower=false, string enemyteam = null){
		DatabaseReference minus_gold_db = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(username).Child("gold");
		if (enemyteam != null) {
			minus_gold_db = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(enemyteam).Child("gold");
			Debug.Log ("musuh");
		}
		minus_gold_db.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Minus Gold");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				Debug.Log("Masuk minus gold");
				/*Dictionary<string, object> results;
				results = (Dictionary<string, object>) task.Result.Value;*/
				int results=int.Parse(task.Result.Value.ToString());
				//int b = int.Parse(results["gold"].ToString())-_gold;
				int b;

				//jika isBuyingTower
				//maka ada kemungkinan diskon
				//else nothing special
				if(isBuyingTower)b=results-(_gold*event_price/100);
				else b=results-_gold;
				Debug.Log(results+" "+_gold);
				//results["gold"]=b.ToString();
				minus_gold_db.RunTransaction(mutableData => {
					mutableData.Value = b.ToString();//results;
					return TransactionResult.Success(mutableData);
				});
			}
		});
	}

	public void SendInvasionResult(int stolenGold, int stolenGem, int[] remainingTroops, string[,] remainingConditionBuildingList=null){
		Debug.Log (enemyteam.ToString ());
		//plusGold(stolenGold);//tambah gold tim
		//minusGold (100, false, enemyteam);//minus gold enemy
		//plusGem(10);// tambah gem tim
		//minusGem(10,enemyteam);//kurangi gem musuh
		//updateTroops(remainingTroops);
		if(remainingConditionBuildingList!=null) addAttackLog(remainingConditionBuildingList,remainingTroops,enemyteam);//building update masih salah
		//setIsAttacked (enemyteam, true);
	}

	public void plusGold(int _gold, string enemyteam = null){
		DatabaseReference plusGoldDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child("gold");
		if (enemyteam != null) {
			plusGoldDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(enemyteam).Child("gold");
			Debug.Log ("musuh");
		}
		plusGoldDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Plus Gold");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				Debug.Log("Masuk minus gold");
				/*Dictionary<string, object> results;
				results = (Dictionary<string, object>) task.Result.Value;*/
				int results=int.Parse(task.Result.Value.ToString());
				//int b = int.Parse(results["gold"].ToString())-_gold;
				int b=results+_gold;
				//results["gold"]=b.ToString();
				plusGoldDB.RunTransaction(mutableData => {
					mutableData.Value = b.ToString();//results;
					return TransactionResult.Success(mutableData);
				});
			}
		});
	}

	public void plusGem(int _gem, string enemyteam=null){
		DatabaseReference plusGemDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child("gem");
		if (enemyteam != null) {
			plusGemDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(enemyteam).Child("gem");
			Debug.Log ("musuh");
		}
		plusGemDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Plus Gem");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				int results = int.Parse(task.Result.Value.ToString());
				int b = results+_gem;
				plusGemDB.RunTransaction(mutableData => {
					mutableData.Value = b.ToString();
					return TransactionResult.Success(mutableData);
				});
			}
		});
	}

	public void minusGem(int _gem, string enemyteam=null){
		DatabaseReference minusGemDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child("gem");
		if (enemyteam != null) {
			minusGemDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(enemyteam).Child("gem");
			Debug.Log ("musuh");
		}
		minusGemDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Minus Gem");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				int results = int.Parse(task.Result.Value.ToString());
				int b = results-_gem;
				minusGemDB.RunTransaction(mutableData => {
					mutableData.Value = b.ToString();
					return TransactionResult.Success(mutableData);
				});
			}
		});
	}

	public void plusTroops(int [] _jumlah){
		DatabaseReference plusTroopsDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child("troop");
		plusTroopsDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Plus Troops");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				Dictionary<string, object> results;
				results = (Dictionary<string, object>) task.Result.Value;
				results["t1"]=(int.Parse(results["t1"].ToString())+_jumlah[0]).ToString();
				results["t2"]=(int.Parse(results["t2"].ToString())+_jumlah[1]).ToString();
				results["t3"]=(int.Parse(results["t3"].ToString())+_jumlah[2]).ToString();
				results["t4"]=(int.Parse(results["t4"].ToString())+_jumlah[3]).ToString();
				results["t5"]=(int.Parse(results["t5"].ToString())+_jumlah[4]).ToString();
				results["t6"]=(int.Parse(results["t6"].ToString())+_jumlah[5]).ToString();
				plusTroopsDB.RunTransaction(mutableData => {
					mutableData.Value = results;
					return TransactionResult.Success(mutableData);
				});
			}
		});
	}

	public void updateTroops(int [] _jumlah){
		DatabaseReference updateTroopsDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child("troop");
		updateTroopsDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Update Troops");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				Dictionary<string, object> results;
				results = (Dictionary<string, object>) task.Result.Value;
				results["t1"]=_jumlah[0].ToString();
				results["t2"]=_jumlah[1].ToString();
				results["t3"]=_jumlah[2].ToString();
				results["t4"]=_jumlah[3].ToString();
				results["t5"]=_jumlah[4].ToString();
				results["t6"]=_jumlah[5].ToString();
				updateTroopsDB.RunTransaction(mutableData => {
					mutableData.Value = results;
					return TransactionResult.Success(mutableData);
				});
			}
		});

	}

	public void GetTroops(){
		DatabaseReference getTroopsDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child("troop");
		getTroopsDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Get Troops");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				Dictionary<string, object> results;

				results = (Dictionary<string, object>) task.Result.Value;
				troops[0]=int.Parse(results["t1"].ToString());
				troops[1]=int.Parse(results["t2"].ToString());
				troops[2]=int.Parse(results["t3"].ToString());
				troops[3]=int.Parse(results["t4"].ToString());
				troops[4]=int.Parse(results["t5"].ToString());
				troops[5]=int.Parse(results["t6"].ToString());
			}
		});
	}

	public void GetOpponent(){
		DatabaseReference opponentDb = FirebaseDatabase.DefaultInstance.GetReference ("tim");
		opponentDb.GetValueAsync().ContinueWith (task => {
			if (task.IsFaulted) {
				Debug.Log ("Error Get Opponent");
				// ERROR HANDLER
			} else if (task.IsCompleted) {
				Dictionary<string, object> results;
				results = (Dictionary<string, object>)task.Result.Value;
				int count = 0;
				foreach (var item in results) {
					if (!item.Key.Equals (username)) {
						detailresults = (Dictionary<string, object>)item.Value;
						opponent [count, 0] = item.Key.ToString ();
						opponent [count, 1] = detailresults["gold"].ToString ();
						opponent [count++, 2] = detailresults["gem"].ToString ();
					}
				}
			}
		});
	}

	public void GetGoldGem(){
		DatabaseReference getGoldGemDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(username);
		getGoldGemDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("username"+username);
				Debug.Log("Error Get Gold Gem");
				// ERROR HANDLER
			}else if (task.IsCompleted) {
				Dictionary<string, object> results;
				results = (Dictionary<string, object>) task.Result.Value;
				gold = int.Parse(results["gold"].ToString());
				gem = int.Parse(results["gem"].ToString());
				buildingNumber = int.Parse(results["buildingnumber"].ToString());
			}
		});
	}

	public void GetGoldGem(string attacked){
		DatabaseReference getGoldGemDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(attacked);
		getGoldGemDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Get Gold Gem Musuh");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				Dictionary<string, object> results;

				results = (Dictionary<string, object>) task.Result.Value;
				enemyteam = attacked;
				enemygold = int.Parse(results["gold"].ToString());
				enemygem = int.Parse(results["gem"].ToString());
			}
		});
	}
	public void setIsAttacked(string attacked, bool flag=false){
		DatabaseReference setAttackedDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(attacked).Child("isAttacked");
		setAttackedDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error SetIsAttacked");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				Debug.Log("Masuk Set IsAttacked");
				string results = task.Result.Value.ToString();
				results = username;
				if (flag){
					results = "";
				}
				setAttackedDB.RunTransaction(mutableData => {
					mutableData.Value = results;//results;
					return TransactionResult.Success(mutableData);
				});
			}
		});
	}

	public void getLastAttacked(string teamname,GameManager.Aksi callback){
        DatabaseReference getLastAttackedDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(teamname).Child("lastAttacked");
        getLastAttackedDB.GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted) {
                Debug.Log("Error Get Last Attacked");
                // ERROR HANDLER
            }else if (task.IsCompleted) {
                Debug.Log("masuk");
                if (task.Result.Value == null){
                    lastAttackedSecs = 301;
                }
                long results = long.Parse(task.Result.Value.ToString());
                System.DateTime epochStart = new System.DateTime(1970,1,1,7,0,0,System.DateTimeKind.Local).AddMilliseconds(results);
                int secondsPassed = (int) (System.DateTime.Now - epochStart).TotalSeconds;
                lastAttackedSecs = secondsPassed;
                callback(lastAttackedSecs);
                //Debug.Log("last attack "+epochStart);
                //Debug.Log("now "+System.DateTime.Now);
                //Debug.Log("last attack in secs "+secondsPassed);
            }
        });
    }

	// public void getLastAttacked(string teamname){
	// 	DatabaseReference getLastAttackedDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(teamname).Child("lastAttacked");
	// 	getLastAttackedDB.GetValueAsync().ContinueWith(task => {
	// 		if (task.IsFaulted) {
	// 			Debug.Log("Error Get Last Attacked");
	// 			// ERROR HANDLER
	// 		}else if (task.IsCompleted) {
	// 			if (task.Result.Value == null || task.Result.Value == ""){
	// 				lastAttackedSecs = 301;
	// 			}
	// 			long results = long.Parse(task.Result.Value.ToString());
	// 			System.DateTime epochStart = new System.DateTime(1970,1,1,7,0,0,System.DateTimeKind.Local).AddMilliseconds(results);
	// 			Debug.Log("last attack "+epochStart);
	// 			Debug.Log("now "+System.DateTime.Now);
	// 			int secondsPassed = (int) (System.DateTime.Now - epochStart).TotalSeconds;
	// 			lastAttackedSecs = secondsPassed;
	// 			Debug.Log("last attack in secs "+secondsPassed);
	// 		}
	// 	});
	// }

	public void GetAttacked(){
		DatabaseReference getAttackedDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(username);
		getAttackedDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Get Attacked");
				// ERROR HANDLER
			}else if (task.IsCompleted) {
				Dictionary<string, object> results;
				results = (Dictionary<string, object>) task.Result.Value;
				isAttacked = results["isAttacked"].ToString();
				lastAttacked = long.Parse(results["lastAttacked"].ToString());
			}
		});
	}
	public void GetAttacked(string attacked){
		DatabaseReference getAttackedDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(attacked);
		getAttackedDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Get Enemy Attacked");
				// ERROR HANDLER
			}else if (task.IsCompleted) {
				Dictionary<string, object> results;
				results = (Dictionary<string, object>) task.Result.Value;
				isEnemyAttacked = results["isAttacked"].ToString();
				lastEnemyAttacked = long.Parse(results["lastAttacked"].ToString());
			}
		});
	}

	public void GetMetadataBuilding(){
		DatabaseReference getMetaDataBuildingDB = FirebaseDatabase.DefaultInstance.GetReference("metadata").Child("tower");
		getMetaDataBuildingDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Get Meta Data Building");
				// ERROR HANDLER
			}else if (task.IsCompleted) {
				//nama tower dan spesifikasinya
				Dictionary<string, object> resultsX = (Dictionary<string, object>) task.Result.Value;//acannon, bmage, etc
				Debug.Log(resultsX.Keys);
				Dictionary<string, object> detailresults2;
				allmetadatatower=new List<List<Dictionary<string, object>>>();
				foreach(var item in resultsX){//level0,level1,level2
					detailresults2 = (Dictionary<string, object>) item.Value;//hp, x, y
					List<Dictionary<string, object>> temp = new List<Dictionary<string, object>>();
					foreach (var item2 in detailresults2){
						Dictionary<string, object> detailresultsPerLevel = (Dictionary<string, object>) item2.Value;
						temp.Add(detailresultsPerLevel);
						Debug.Log("sini"+detailresultsPerLevel["hp"].ToString());
					}
					allmetadatatower.Add(temp);
				}
				int towertype=0;
				int towerlevel=0;
				foreach(List<Dictionary<string,object>> items in allmetadatatower){
					towerlevel = 0;
					foreach (Dictionary<string,object> item in items) {
						towerCost[towertype] = int.Parse(item["buy"].ToString());

						HP[towertype,towerlevel] = float.Parse(item["hp"].ToString());

						Debug.Log("sini2"+HP[towertype,towerlevel].ToString());
						Attack[towertype,towerlevel] = float.Parse(item["attack"].ToString());
						AttackSpeed[towertype,towerlevel] = float.Parse(item["attackspeed"].ToString());
						upgrade[towertype,towerlevel] = int.Parse(item["upgrade"].ToString());
						repair[towertype,towerlevel] = int.Parse(item["repair"].ToString());
						sell[towertype,towerlevel] = int.Parse(item["sell"].ToString());
						Range[towertype,towerlevel++] = float.Parse(item["range"].ToString());
					}
					towertype++;
				}
			}
		});
	}

	public void GetMetadataTroop(){
		DatabaseReference getMetaDataTroopDB = FirebaseDatabase.DefaultInstance.GetReference("metadata").Child("troop");
		getMetaDataTroopDB.GetValueAsync().ContinueWith(task => {
			if (task.IsCompleted) {
				allmetadata=new List<Dictionary<string, object>>();
				results = (Dictionary<string, object>) task.Result.Value;
				foreach(var item in results){
					detailresults = (Dictionary<string, object>) item.Value;
					allmetadata.Add(detailresults);
				}
			}
		});
	}

	public void AddBuildingNumber(){
		DatabaseReference add_number_db = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(username).Child("buildingnumber");
		add_number_db.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error change building number");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				Debug.Log("Masuk change building number");
				int result=int.Parse(task.Result.Value.ToString());
				int c=result+1;
				add_number_db.RunTransaction(mutableData => {
					mutableData.Value = c.ToString();//results;
					return TransactionResult.Success(mutableData);
				});
			}
		});
	}
	public void buyTower(int harga){
		// db = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username);
		// db.GetValueAsync().ContinueWith(task => {
		// if (task.IsFaulted) {
		// Debug.Log("Error Buy Tower");
		// // ERROR HANDLER
		// }
		// else if (task.IsCompleted) {
		// Debug.Log("masuk buy Tower");
		// Dictionary<string, object> results;
		// results = (Dictionary<string, object>) task.Result.Value;
		// int a = int.Parse(results["buildingnumber"].ToString())+1;
		// int b = int.Parse(results["gold"].ToString())-harga;//-(harga*event_price/100);
		// results["buildingnumber"]=a.ToString();
		// results["gold"]=b.ToString();
		// Debug.Log(b);
		// db.RunTransaction(mutableData => {
		// mutableData.Value = results;
		// return TransactionResult.Success(mutableData);
		// });
		// }
		// });
		// minusGold(harga);
		// AddBuildingNumber();
	}

	public void updateTower(string enemy, string _namatower, string _hp){
		DatabaseReference updateTowerDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(enemy).Child ("tower").Child (_namatower);
		updateTowerDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Update Tower");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				results = (Dictionary<string, object>) task.Result.Value;
				results["hp"]=_hp;
				Debug.Log(results["hp"]);
				updateTowerDB.RunTransaction(mutableData => {
					mutableData.Value = results;
					return TransactionResult.Success(mutableData);
				});
			}

		});
	}

	public void updateBuildingData(string [,] buildingList, string enemyteam=null){
		DatabaseReference updateBuildingDataDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(enemyteam).Child("tower");
		Debug.Log (enemyteam);
		updateBuildingDataDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Update Building Data");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				results = (Dictionary<string, object>) task.Result.Value;
				for (int i=0; i<buildingList.GetLength(0); i++){
					Debug.Log("awal "+((Dictionary<string, object>) results[buildingList[i,0]])["hp"]);
					((Dictionary<string, object>) results[buildingList[i,0]])["hp"]=buildingList[i,2];
					Debug.Log("to be updated "+((Dictionary<string, object>) results[buildingList[i,0]])["hp"]);
					//updateTower(enemyteam,buildingList[i,0],buildingList[i,2]);
				}
				updateBuildingDataDB.RunTransaction(mutableData => {
					mutableData.Value = results;
					return TransactionResult.Success(mutableData);
				});
			}
		});
	}

	public void addAttackLog(string [,] buildingList, int [] troops, string enemyteam=null){
		string stringTroop = '"'+"troop"+'"'+":{";
		stringTroop = string.Concat (stringTroop, string.Format (
			'"' + "t1" + '"' + ":{0}," +
			'"' + "t2" + '"' + ":{1}," +
			'"' + "t3" + '"' + ":{2}," +
			'"' + "t4" + '"' + ":{3}," +
			'"' + "t5" + '"' + ":{4}," +
			'"' + "t6" + '"' + ":{5}"
			,troops[0],troops[1],troops[2],troops[3],troops[4],troops[5]));
		stringTroop = stringTroop + "}";
		System.DateTime epochStart = new System.DateTime (1970, 1, 1, 7, 0, 0, System.DateTimeKind.Local);
		int unixTime = (int)(System.DateTime.Now - epochStart).TotalSeconds;
		string key = enemyteam + '_' + unixTime;
		Debug.Log (key);
		DatabaseReference addAttackLogDB = FirebaseDatabase.DefaultInstance.GetReference ("attackLog").Child(key);
		string stringData = "{"+'"'+"timAttack"+'"'+":"+'"'+username+'"'+","+'"'+"timAttacked"+'"'+":"+'"'+enemyteam+'"'+","+'"'+"gold"+'"'+":"+1000+","+'"'+"gem"+'"'+":"+10+","+'"'+"tower"+'"'+":{";
		for (int i = 0; i < buildingList.GetLength(0); i++) {
			stringData = stringData + '"' + buildingList [i, 0] + '"' + ":{";
			stringData = string.Concat (stringData, string.Format (
				'"'+"arah"+'"'+":{0},"+
				'"'+"hp"+'"'+":{1},"+
				'"'+"level"+'"'+":{2},"+
				'"'+"type"+'"'+":{3},"+
				'"'+"x"+'"'+":{4},"+
				'"'+"y"+'"'+":{5}"
				, buildingList [i, 1],buildingList [i, 2],buildingList [i, 3],buildingList [i, 4],buildingList [i, 5],buildingList [i, 6]));
			if (i != buildingList.GetLength (0) - 1) {
				stringData = stringData + "},";
			} else {
				stringData = stringData + "}";
			}
		}
		stringData = stringData + "},"+stringTroop+"}";
		Debug.Log (stringData);
		addAttackLogDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Add Attack Log Data");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				//results = (Dictionary<string, object>) task.Result.Value;
				addAttackLogDB.RunTransaction(mutableData => {
					mutableData.Value = stringData;
					return TransactionResult.Success(mutableData);
				});
			}
		});
	}

	public void addTower(int arah, int type, int x, int y, int number, int harga){
		DatabaseReference addTowerDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child ("tower");
		addTowerDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Add Tower");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				//minusGold(harga);
				GameManager gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();
				results = (Dictionary<string, object>) task.Result.Value;
				string nama="tower"+number;
				Dictionary<string, object> detail = new Dictionary<string, object>();
				detail.Add("arah",arah);
				detail.Add("hp",HP[type,0]);
				detail.Add("level",0);
				detail.Add("type",type);
				detail.Add("x",x);
				detail.Add("y",y);
				results.Add(nama,detail);
				addTowerDB.RunTransaction(mutableData => {
					mutableData.Value = results;
					return TransactionResult.Success(mutableData);
				});
				//buyTower(harga);
				minusGold(harga,true);
				AddBuildingNumber();
			}
		});
	}

	public void deleteTower(string _namatower, int _gold){
		DatabaseReference deleteTowerDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child ("tower");
		deleteTowerDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Delete Tower");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				results = (Dictionary<string, object>) task.Result.Value;
				results.Remove(_namatower);
				Debug.Log(_namatower);
				deleteTowerDB.RunTransaction(mutableData => {
					mutableData.Value = results;
					return TransactionResult.Success(mutableData);
				});
				plusGold(_gold);
			}
		});
	}

	public void upgradeTower(string _namatower, int _gold){
		DatabaseReference upgradeTowerDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child ("tower").Child (_namatower);
		upgradeTowerDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Upgrade Tower");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				GameManager gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();
				results = (Dictionary<string, object>) task.Result.Value;
				results["level"]=(int.Parse(results["level"].ToString())+1).ToString();
				results["hp"]=HP[int.Parse(results["type"].ToString()),int.Parse(results["level"].ToString())];
				upgradeTowerDB.RunTransaction(mutableData => {
					mutableData.Value = results;
					return TransactionResult.Success(mutableData);
				});
				minusGold(_gold);
			}
		});
	}

	public void repairTower(string _namatower, int _gold){
		DatabaseReference repairTowerDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(username).Child ("tower").Child (_namatower);
		repairTowerDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Repair Tower");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				
				GameManager gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();
				results = (Dictionary<string, object>) task.Result.Value;
				results["hp"]=HP[int.Parse(results["type"].ToString()),int.Parse(results["level"].ToString())].ToString();
				repairTowerDB.RunTransaction(mutableData => {
					mutableData.Value = results;
					return TransactionResult.Success(mutableData);
				});
				minusGold(_gold);
			}

		});
	}

	public void GetBuildingData(){
		DatabaseReference getBuildingDataDB = FirebaseDatabase.DefaultInstance.GetReference("tim").Child(username).Child("tower");
		getBuildingDataDB.GetValueAsync().ContinueWith(task => {
			if (task.IsCompleted) {
				allResultList=new List<Dictionary<string, object>>();
				results = (Dictionary<string, object>) task.Result.Value;
				foreach(var item in results){
					detailresults = (Dictionary<string, object>) item.Value;
					namatower.Add(item.Key);
					allResultList.Add(detailresults);
				}
				SceneManager.LoadScene (1);
			}
		});
	}

	public void GetBuildingData(string attacked){
		namatower.Clear ();
		DatabaseReference getBuildingDataDB = FirebaseDatabase.DefaultInstance.GetReference ("tim").Child(attacked).Child("tower");
		getBuildingDataDB.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("Error Get Building Musuh");
				// ERROR HANDLER
			}
			else if (task.IsCompleted) {
				allResultList=new List<Dictionary<string, object>>();
				results = (Dictionary<string, object>) task.Result.Value;
				foreach(var item in results){
					detailresults = (Dictionary<string, object>) item.Value;
					namatower.Add(item.Key);
					allResultList.Add(detailresults);
				}
				SceneManager.LoadScene (2);
			}
		});
		GameObject.Find ("Canvas").GetComponent<LoadingBar> ().LoadLevel(2);
	}
	
	public void SendLogout(string idToken){
        StartCoroutine("Logout",idToken);
    }
    public IEnumerator Logout(string idToken){
        WWWForm form = new WWWForm();
        form.AddField("idToken", idToken);

        UnityWebRequest www = UnityWebRequest.Post("https://us-central1-irgl2018.cloudfunctions.net/irgl_auth_logout", form);
        yield return www.Send ();

        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
            Debug.Log(www.url);
        }
        else {
			if(www.responseCode==200){
				UnlistenAllChange();
				auth.SignOut();
				
				PlayerPrefs.DeleteKey("username");
				PlayerPrefs.DeleteKey("password");
				PlayerPrefs.DeleteKey("token");
				SceneManager.LoadScene (0);
			}
        }
		
		//StopCoroutine("Logout");
    }
    public void logoutFromFirebase(){
        auth.CurrentUser.TokenAsync(true).
            ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("TokenAsync was canceled.");
                    return;
                }

                if (task.IsFaulted) {
                    Debug.LogError("TokenAsync encountered an error: " + task.Exception);
                    return;
                }

                string idToken = task.Result;
				//Debug.Log(idToken);
                SendLogout(idToken);
            });
    }

	void UnlistenAllChange(){
		unlistenToEventPriceChange();
		unlistenToGoldChange();
		unlistenToGemChange();
		unlistenToIsAttackedChange();
	}
}