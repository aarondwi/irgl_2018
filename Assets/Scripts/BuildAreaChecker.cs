﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildAreaChecker : MonoBehaviour {
	[HideInInspector] public int collisionQuantity;
	private TowerController tc;

	void Start(){
		tc = transform.parent.gameObject.GetComponent<TowerController> ();
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "BuildingArea") {
			if (collisionQuantity == 0 && !tc.isPlacementDone) {
				tc.ModifyPlacementPermission(false);
			}
			collisionQuantity++;
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.tag == "BuildingArea") {
			collisionQuantity--;
			if (collisionQuantity == 0 && !tc.isPlacementDone) {
				tc.ModifyPlacementPermission(true);
			}
		}
	}
}