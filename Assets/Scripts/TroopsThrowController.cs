﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TroopsThrowController : MonoBehaviour {

	private bool isDestroyed = false;
	public float throwDmgNormal;
	public float throwDmgWall;

	void Update () {
		if (isDestroyed) {
			Destroy (gameObject);
		}
	}
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "BuildingBody") {
			TowerController tc=other.gameObject.GetComponentInParent<TowerController> ();
			if(tc.buildingCode!=4) tc.Hit(throwDmgNormal);
			else tc.Hit (throwDmgWall);
			isDestroyed = true;
		}else if(other.gameObject.tag == "ThrowableLimit"){
			isDestroyed = true;
		}
	}
}
